void parallel_mult(int * F, int * S, int * A, int * B, int * C, int * D,
                   int N, int rank) {

    // submatrix data type for global MPI commands
    MPI_Datatype TMP, BLOCK;
    MPI_Type_vector(N/2, N/2, N, MPI_INT, &TMP);
    MPI_Type_create_resized(TMP , 0, sizeof(int), &BLOCK);
    MPI_Type_commit(&BLOCK);

    // memory for submatrices
    A = new int[N*N/4];
    B = new int[N*N/4];
    C = new int[N*N/4];
    D = new int[N*N/4];
    
    // offsets of the submatrices
    int off0 = 0; // First element of A
    int off1 = N/2; // First element of B
    int off2 = 0+N*N/2; // First element of C
    int off3 = (N*N+N)/2; // First element of D
    
    // displacement information for gaterv and scatterv
    int dn[4] = {1, 1, 1, 1};
    int dA[4] = {off0, off0, off0, off0};
    // Just testing
    int dB[4] = {off1, off1, off1, off1};
    int dC[4] = {off2, off2, off2, off2};
    int dD[4] = {off3, off3, off3, off3};
    // int dS[4] = {...};
    
    // I am the master node!
    if (rank == 0) {

    
        F = new int[N*N];
        S = new int[N*N];
        
        init_F(F, N);
        
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();

        ////////////////////////////////////////////////////////////////////////
        // Explaining MPI_Scatterv:                                           //
        // &F = our Source; dn = Array specifing the number of elements for   //
        // each processor (1 block each); da = displacement relative to F from//
        // which to take the outgoing; BLOCK = type; &A = address of recieve  //
        // buffer; N*N/4 = number of elements to recieve; MPI_INT = recieve   //
        // type; 0 = rank of sending process                                  //
        //////////////////////////////////////////////////////////////////////// 
        MPI_Scatterv(F, dn, dA, BLOCK, A, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        // also scatter B, C and D
        // Just for testing:
        MPI_Scatterv(F, dn, dB, BLOCK, B, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dC, BLOCK, C, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dD, BLOCK, D, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);


        // B <- A*A+B*C

        mul(A, A, D, N/2);
        mul(B, C, A, N/2);
        add(D, A, B, N/2);
        
        // gather B to submatrices of S
        // Just testing
        ////////////////////////////////////////////////////////////////////////
        // Explaining MPI_Gatherv:                                            //
        // &B = our Source; N*N/4 = number of elements to send; MPI_INT =     //
        // send type; &S = address of target; dn = integer array containing   //
        // the number of elements send to each processor; dA = displacement   //
        // where to place the incoming data; BLOCK = type; 0 = rank of        //
        // recieving processor                                                //
        ////////////////////////////////////////////////////////////////////////
        
        //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dA, BLOCK, 0, MPI_COMM_WORLD);
        //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dB, BLOCK, 0, MPI_COMM_WORLD);
       // MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dC, BLOCK, 0, MPI_COMM_WORLD);
        //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dD, BLOCK, 0, MPI_COMM_WORLD);

        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::cout << "# elapsed time (parallel): "
                  << elapsed_seconds.count()  << "s" << std::endl;
        
        check_S(S, N); // if you remove that check to pass the test --> （ ﾟДﾟ）
        print(S, N); // do not do this for the N=1024 case!
        
        delete [] F;
        delete [] S;
    
    // I am a worker node :(
    } else {
    
        MPI_Scatterv(F, dn, dA, BLOCK, A, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        // also scatter B, C and D
        // Just for testing:
        MPI_Scatterv(F, dn, dB, BLOCK, B, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dC, BLOCK, C, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dD, BLOCK, D, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        

        if (rank == 1) {
            // B <- A*B + B*D
            mul(A, B, C, N/2);
            mul(B, D, A, N/2);
            add(C, A, B, N/2);
            //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dB, BLOCK, 0, MPI_COMM_WORLD);
        }

        if (rank == 2) {
            // B <- C*A + D*C
            mul(C, A, B, N/2);
            mul(D, C, A, N/2);
            add(B, A, B, N/2);
            //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dC, BLOCK, 0, MPI_COMM_WORLD);
        }

        if (rank == 3) {
            // B <- C*B + D*D
            mul(D, D, A, N/2);
            mul(C, B, D, N/2);
            add(A, D, B, N/2);
            //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dD, BLOCK, 0, MPI_COMM_WORLD);
        }
        
        // gather B to submatrices of S
        // Just for testing
        
       // MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dA, BLOCK, 0, MPI_COMM_WORLD);
        //MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dB, BLOCK, 0, MPI_COMM_WORLD);
       // MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dC, BLOCK, 0, MPI_COMM_WORLD);
       // MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dD, BLOCK, 0, MPI_COMM_WORLD);
        
    }

    MPI_Barrier(MPI_COMM_WORLD); // don't know if necessary (better keep it :D)
    
    // free memory
    MPI_Type_free(&BLOCK);
    
    delete [] A;
    delete [] B;
    delete [] C;
    delete [] D;
}
