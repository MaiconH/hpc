#include <iostream>
#include <chrono> 
#include <mpi.h>
#include <fstream>

#define NUM_ROUNDS (1000)
////////////////////////////////////////////////////////////////////////////////
// Compile with mpic++ -o mmulti mmulti.cpp -std=c++11                        //
////////////////////////////////////////////////////////////////////////////////


void init_F(int * F, int N) {

    //synthesize first discrete derivative
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if (i == j)
                F[i*N+j] = -1;
            else if (i+1 == j)
                F[i*N+j] = +1;
            else
                F[i*N+j] = 0;
}

void state_error(int i, int j) {
    std::cout << "error at entry (" 
              << i << ", " << j << ")" << std::endl;
}

void check_S(int * S, int N) {

    //check for second discrete derivative
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++) {
        
            int value = S[i*N+j];
        
            if (i == j)
                if (value != 1) {
                    state_error(i, j);
                    return;
                } else continue;
           
            else if (i+1 == j)
                if(value != -2) {
                    state_error(i, j);
                    return;
                } else continue;
            
            else if (i+2 == j)
                if(value != 1) {
                    state_error(i, j);
                    return;
                } else continue;
                
            else 
                if (value != 0) {
                    state_error(i, j);
                    return;
                }
        }
}

void add(const int * R, const int * S, int * T, int N) {

    // T = R+S
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++) {
            T[i*N+j] = R[i*N+j]+S[i*N+j];
        }
}

void mul(const int * R, const int * S, int * T, int N) {

    // T = R*S
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++) {

            int sum = 0;
            for (int k = 0; k < N; k++)
                sum += R[i*N+k]*S[k*N+j];
            T[i*N+j] = sum;
        }
}

void print(const int * R, int N) {
 
    // don't do that for huge Ns
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            std::cout << R[i*N+j] << "\t" << (j == N-1 ? "\n" : " ");
    std::cout << std::endl;
}

void sequential_mult(int * F, int * S, int N, int rank) {

    if (rank == 0) {

        F = new int[N*N];
        S = new int[N*N];
    
        init_F(F, N);                       // write first derivative

        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();

        mul(F, F, S, N);                   // calculate second derivative

        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        /*std::cout << "# elapsed time (sequential): "
                  << elapsed_seconds.count()  << "s" << std::endl;
        */
        std::ofstream mmultiTable;
        mmultiTable.open ("mmultiTable", std::ios_base::app);
        mmultiTable << elapsed_seconds.count() << ";";
        mmultiTable.close();
        
        //check_S(S, N);                      // check for second derivative
        delete [] F;
        delete [] S;
    }
}

/*
 * TODO
 */

void parallel_mult(int * F, int * S, int * A, int * B, int * C, int * D,
                   int N, int rank) {

    // submatrix data type for global MPI commands
    MPI_Datatype TMP, BLOCK;
    MPI_Type_vector(N/2, N/2, N, MPI_INT, &TMP);
    MPI_Type_create_resized(TMP , 0, sizeof(int), &BLOCK);
    MPI_Type_commit(&BLOCK);

    // memory for submatrices
    A = new int[N*N/4];
    B = new int[N*N/4];
    C = new int[N*N/4];
    D = new int[N*N/4];
    
    // offsets of the submatrices
    int off0 = 0; // First element of A
    int off1 = N/2; // First element of B
    int off2 = 0+N*N/2; // First element of C
    int off3 = (N*N+N)/2; // First element of D
    
    // displacement information for gaterv and scatterv
    int dn[4] = {1, 1, 1, 1};
    int dA[4] = {off0, off0, off0, off0};
    // Just testing
    int dB[4] = {off1, off1, off1, off1};
    int dC[4] = {off2, off2, off2, off2};
    int dD[4] = {off3, off3, off3, off3};
    // int dS[4] = {...};
    int dS[4] = {off0, off1, off2, off3};
    // I am the master node!
    if (rank == 0) {

    
        F = new int[N*N];
        S = new int[N*N];
        
        init_F(F, N);
        
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();

        ////////////////////////////////////////////////////////////////////////
        // Explaining MPI_Scatterv:                                           //
        // F = our Source; dn = Array specifing the number of elements for    //
        // each processor (1 block each); da = displacement relative to F from//
        // which to take the outgoing; BLOCK = type; A = address of recieve   //
        // buffer; N*N/4 = number of elements to recieve; MPI_INT = recieve   //
        // type; 0 = rank of sending process                                  //
        //////////////////////////////////////////////////////////////////////// 
        MPI_Scatterv(F, dn, dA, BLOCK, A, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        // also scatter B, C and D
        MPI_Scatterv(F, dn, dB, BLOCK, B, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dC, BLOCK, C, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dD, BLOCK, D, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);


        // B <- A*A+B*C

        mul(A, A, D, N/2);
        mul(B, C, A, N/2);
        add(D, A, B, N/2);
        
        // gather B to submatrices of S
        ////////////////////////////////////////////////////////////////////////
        // Explaining MPI_Gatherv:                                            //
        // B = our Source; N*N/4 = number of elements to send; MPI_INT =      //
        // send type; S = address of target; dn = integer array containing    //
        // the number of elements send to each processor; dA = displacement   //
        // where to place the incoming data; BLOCK = type; 0 = rank of        //
        // recieving processor                                                //
        ////////////////////////////////////////////////////////////////////////
        
        MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dS, BLOCK, 0, MPI_COMM_WORLD);

        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        /*std::cout << "# elapsed time (parallel): "
                  << elapsed_seconds.count()  << "s" << std::endl;
        */
        std::ofstream mmultiTable;
        mmultiTable.open ("mmultiTable", std::ios_base::app);
        mmultiTable << elapsed_seconds.count() << "\n";
        mmultiTable.close();
        //check_S(S, N); // if you remove that check to pass the test --> （ ﾟДﾟ）
        //print(S, N); // do not do this for the N=1024 case!
        
        delete [] F;
        delete [] S;
    
    // I am a worker node :(
    } else {
    
        MPI_Scatterv(F, dn, dA, BLOCK, A, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        // also scatter B, C and D
        MPI_Scatterv(F, dn, dB, BLOCK, B, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dC, BLOCK, C, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatterv(F, dn, dD, BLOCK, D, N*N/4, MPI_INT, 0, MPI_COMM_WORLD);
        

        if (rank == 1) {
            // B <- A*B + B*D
            mul(A, B, C, N/2);
            mul(B, D, A, N/2);
            add(C, A, B, N/2);      
        }

        if (rank == 2) {
            // B <- C*A + D*C
            mul(C, A, B, N/2);
            mul(D, C, A, N/2);
            add(B, A, B, N/2);
        }

        if (rank == 3) {
            // B <- C*B + D*D
            mul(D, D, A, N/2);
            mul(C, B, D, N/2);
            add(A, D, B, N/2);
        }
        
        // gather B to submatrices of S
        MPI_Gatherv(B, N*N/4, MPI_INT, S, dn, dS, BLOCK, 0, MPI_COMM_WORLD);
    }

    MPI_Barrier(MPI_COMM_WORLD); // don't know if necessary (better keep it :D)
    
    // free memory
    MPI_Type_free(&BLOCK);
    
    delete [] A;
    delete [] B;
    delete [] C;
    delete [] D;
}

/*
 * END TODO
 */
int main() {

    int rank, size; // size will be hardcoded to 4
    int N = 1024;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int * F = nullptr,
        * S = nullptr,
        * A = nullptr,
        * B = nullptr,
        * C = nullptr,
        * D = nullptr;
    for(int i=0; i<NUM_ROUNDS; i++) {
        // first of all, we try it with one processor 
        sequential_mult(F, S, N, rank);
    
        // now, let's do it with four processors
        parallel_mult(F, S, A, B, C, D, N, rank);
    }
    
    if (rank == 0)
        std::cout << "programme terminated" << std::endl;

    MPI_Finalize();
}