\contentsline {section}{1 Speedup in Relation to Optimal Sequential Code (2P)}{3}
\contentsline {section}{2 Deadlocks in MPI (6P)}{3}
\contentsline {subsection}{Does the code have the potential to cause a deadlock?}{3}
\contentsline {subsection}{A solution to avoid the deadlock.}{3}
\contentsline {section}{3 Matrix Multiplication using Submatrix Scattering (12P)}{4}
\contentsline {subsection}{(i) A sketch of the partitioning scheme of S for N=10 and a calculation of the position of all entries in A, B, C and D.}{4}
\contentsline {subsection}{(ii) Explanation for the BLOCK data type.}{4}
\contentsline {subsection}{(iii) MPI\_Scatterv and MPI\_Gatherv briefly explained.}{5}
\contentsline {subsection}{(iv) Extension of the SAUCE template with MPI\_Scatterv and MPI\_Gatherv.}{7}
\contentsline {subsection}{(v) Speedup and efficiency.}{9}
