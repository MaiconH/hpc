 
for (i = 0; i < N/2; i++) // N is the width of S.
    for(j = 0; j < N/2; j++) // N is the height of S.
    {
        A[i*N/2+j] = S[i*N+j]; // just the index beginning with 0
        B[i*N/2+j] = S[i*N+j+N/2]; // N/2 is the offset for B
        C[i*N/2+j] = S[(i+N/2)*N+j]; // N*N/2 is the offset for C
        D[i*N/2+j] = S[(i+N/2)*N+j+N/2]; // N*N/2+N/2 is the offset for D
    }        