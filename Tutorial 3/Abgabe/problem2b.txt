To prevent this code from a deadlock, we have to change either the order of sending and recieving for
processor with rank 0 or with rank 1. If we change the order for both processors, then both of them
wait to recieve their integers and we could have a deadlock again.