#include <iostream>
#include <vector>
#include <cmath>

#include <assert.h>
#include <omp.h>

#define X (0)
#define Y (1)
#define G (1E-3)
#define NUM_PARTICLES (10)
#define NUM_THREADS (6)

using namespace std;

// this struct has no purpose except to make the code look like on the slides
template <class T>
struct pair_t {
    T x;
    T y;
    pair_t() : x(0), y(0) {}
    T& operator[](size_t index){ return index == 0 ? x : y;}
    const T& operator[](size_t index) const { return index == 0 ? x : y;}
};


template <class T>
void initial_conditions(vector<pair_t<T>>& positions, 
                        vector<pair_t<T>>& velocities,
                        vector<T>& masses, T central_factor=1E3) {

    assert (positions.size() == velocities.size() &&
            positions.size() == masses.size());

    // initializes NUM_PARTICLES-1 planets around a massive center
    for (size_t q = 0; q < positions.size(); q++) { 
        positions [q][X] =  q*cos(q);
        positions [q][Y] =  q*sin(q);
        velocities[q][X] =  positions [q][Y]/std::max<T>(1, q*q);
        velocities[q][Y] = -positions [q][X]/std::max<T>(1, q*q);
        masses[q] = (q == 0) ? central_factor : static_cast<T>(1);
    }
}


template <class T>
void integrate_system(vector<pair_t<T>>& positions,
                      vector<pair_t<T>>& velocities, 
                      vector<T>& masses, T h=1E-4, size_t timesteps=1L<<18) {

    assert (positions.size() == velocities.size() &&
            positions.size() == masses.size());

    // forces and thread-local forces
    vector<pair_t<T>> forces(positions.size());
    vector<vector<pair_t<T>>> loc_forces(NUM_THREADS);
    for (auto& entry : loc_forces)
        entry = std::vector<pair_t<T>>(positions.size());

    # pragma omp parallel num_threads(NUM_THREADS)
    {
        // remember rank of each thread
        size_t my_rank = omp_get_thread_num();

        for (size_t step = 0; step < timesteps; step++){
             loc_forces[my_rank] = std::vector<pair_t<T>>(positions.size());

            # pragma omp for
            for (size_t q = 0; q < positions.size(); q++)
                for (size_t k = q+1; k < positions.size(); k++) {                   
                    T x_diff = positions[q][X]-positions[k][X];
                    T y_diff = positions[q][Y]-positions[k][Y];
                    T dist = sqrt(x_diff*x_diff + y_diff*y_diff);
                    T dist_cubed = dist*dist*dist;
                    
                    
                    //calcualte local forces
                }
            
            // accumulate the forces
            # pragma omp for 
            for (size_t q = 0; q < positions.size(); q++) {
            }

            # pragma omp for
            for (size_t q = 0; q < positions.size(); q++) {
                // update velocities and positions using explicit Euler (eeew!)
                positions[q][X]  += h*velocities[q][X];
                positions[q][Y]  += h*velocities[q][Y];
                velocities[q][X] += h*forces[q][X]/masses[q];
                velocities[q][Y] += h*forces[q][Y]/masses[q];
            }

            // remove this if you don't like output
            # pragma omp single
            if (step % 1000 == 0) {
                std::cout << "#" << step << " ";
                for (auto& entry : positions)
                    std::cout << entry[X] << " " << entry[Y] << " ";
                std::cout << std::endl;
            }
        }       
    }
}


int main () {

    /** visualize planetary orbits with
    import numpy as np
    import pylab as pl

    data = []
    with open("coordinates", "r") as f:
        for line in f:
            data.append(map(float, line.split()[1:]));

    data = np.array(data).T

    for i in range(data.shape[0]/2):
        pl.plot(data[2*i], data[2*i+1])

    pl.show()
    **/

    vector<double> masses (NUM_PARTICLES);
    vector<pair_t<double>> positions (NUM_PARTICLES), 
                           velocities(NUM_PARTICLES);
    
    
    initial_conditions(positions, velocities, masses);
    integrate_system(positions, velocities, masses);

    std::cout << "ubi materia ibi geometria (Kepler)" << std::endl;
}