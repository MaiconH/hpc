#include <upc.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <chrono>


#define N 100
#define alpha 2.0

shared [1] float x[N*THREADS];
shared [1] float y[N*THREADS];

void axpy() {
    int i, bs = N/THREADS;

    /******************************************
    /* NOT MODIFY THE FOLLOWING LOOP!!!
    /* Sorry for modifying. I wanted the runtime for the loop. 
    ******************************************/
    for(i=MYTHREAD; i<N*THREADS; i+=THREADS){
        x[i] = i;
        y[i] = (N-i);
    }

    /******************************************
    /* THIS IS THE SECTION TO MODIFY!!!
    ******************************************/
    //  upc_forall(i=0; i<N*THREADS; i++; i){
    for(i=MYTHREAD; i<N*THREADS; i+=THREADS) {
        y[i] += alpha*x[i];
    }
    /******************************************
    /* END OF THE SECTION TO MODIFY!!!
    ******************************************/

    /******************************************
    /* NOT MODIFY THE FOLLOWING CODE!!!
    /* Sorry for modifying. I wanted the runtime for the loop. 
    ******************************************/
    upc_barrier;    
}


int main() {
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
        
    for(int j=0; j<100; j++) {
        start = std::chrono::system_clock::now();
        axpy();
        end  = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        if(MYTHREAD == 0)
            std::cout << elapsed_seconds.count() << " s;\n";
    }
    
   if(MYTHREAD == 0){
      int num_errors = 0;
      for(i=0; i<N*THREADS; i++){
         if(y[i] != (N-i)+alpha*i){
            printf("ERROR IN POSITION %d (%f != %f)\n", i, y[i], (N-i)+alpha*i);
            num_errors++;
         }
      }
      if(num_errors){
         printf("%d ERRORS FOUND\n", num_errors);
      } else{
         printf("CORRECT TEST\n");
      } 
   }
}
