#include <upc.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define N 100
#define alpha 2.0

shared [1] float x[N*THREADS];
shared [1] float y[N*THREADS];

int main() {
   int i, bs = N/THREADS;

   /******************************************
   /* NOT MODIFY THE FOLLOWING LOOP!!!
   ******************************************/
   for(i=MYTHREAD; i<N*THREADS; i+=THREADS){
      x[i] = i;
      y[i] = (N-i);
   }

   /******************************************
   /* THIS IS THE SECTION TO MODIFY!!!
   ******************************************/
   upc_forall(i=0; i<N*THREADS; i++; i/THREADS){
      y[i] += alpha*x[i];
   }
   /******************************************
   /* END OF THE SECTION TO MODIFY!!!
   ******************************************/

   /******************************************
   /* NOT MODIFY THE FOLLOWING CODE!!!
   ******************************************/
   upc_barrier;    

   if(MYTHREAD == 0){
      int num_errors = 0;
      for(i=0; i<N*THREADS; i++){
         if(y[i] != (N-i)+alpha*i){
            printf("ERROR IN POSITION %d (%f != %f)\n", i, y[i], (N-i)+alpha*i);
            num_errors++;
         }
      }
      if(num_errors){
         printf("%d ERRORS FOUND\n", num_errors);
      } else{
         printf("CORRECT TEST\n");
      } 
   }
}
