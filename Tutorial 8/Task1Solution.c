#include <algorithm>
#include <iostream>
#include <limits> 
#include <chrono>
#include <cmath>
#include <omp.h>

#define INFTY (std::numeric_limits<double>::infinity())
#define AT(i,j) ((i)*(N+1)+(j))
#define ROUNDS (1000)
// Save A B C D after each other First all A, than B etc.
double* time_elapsed = new double[ROUNDS*4];

void init_matrix(double * matrix, const size_t M, const size_t N) {
     matrix[AT(0, 0)] = 0.0;
    #pragma omp parallel
    #pragma omp for
    for (size_t j = 1; j < N+1; j++)
        matrix[AT(0, j)] = INFTY;
    #pragma omp for
    for (size_t i = 1; i < M+1; i++)
        matrix[AT(i, 0)] = INFTY;
}

double relax_A(double * Q, double * S, const size_t M, const size_t N) {
    std::vector<double> matrix((M+1)*(N+1));
    init_matrix(matrix.data(), M, N);
    
    for (size_t i = 1; i < M+1; i++) 
        #pragma omp parallel for
        for (size_t j = 1; j < N+1; j++) {
            double bsf = matrix[AT(i-1, j-1)];
            if (i > 1)
                bsf = std::min<double>(bsf, matrix[AT(i-2, j-1)]);
            if (j > 1)
                bsf = std::min<double>(bsf, matrix[AT(i-1, j-2)]);              
            matrix[AT(i,j)] = bsf + (Q[i-1]-S[j-1])*(Q[i-1]-S[j-1]);  
        }
    return matrix[AT(M, N)];
}

double relax_B(double * Q, double * S, const size_t M, const size_t N) {
    std::vector<double> matrix((M+1)*(N+1));
    init_matrix(matrix.data(), M, N);

    // column_beginning has to increase as long as slices < N (N = row_width)
    // row_beginning has to increase after slices reach N
    
    size_t column_beginning = 0;
    size_t row_beginning = 1;
    size_t column_end = 0;
    // Gain a little bit speed by creating threads now.
    #pragma omp parallel 
    for(size_t slices = 1; slices < (M+N); slices++) {     
        
        #pragma omp single      
        if(slices < N+1)
            column_beginning++;
        else
            row_beginning++;
        
        #pragma omp single 
        if(slices > M)
            column_end++;
        
        unsigned int i = row_beginning;
        
        // Scheduling has no positive effect :(
        #pragma omp for 
        for(unsigned int j = column_beginning; j > column_end; j--) {
            matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
        }
    }
    return matrix[AT(M, N)];
}

double relax_D(double * Q, double * S, const size_t M, const size_t N) {
    std::vector<double> matrix((M+1)*(N+1));
    init_matrix(matrix.data(), M, N);

    // column_beginning has to increase as long as slices < N (N = row_width)
    // row_beginning has to increase after slices reach N
    
    size_t column_beginning = 0;
    size_t row_beginning = 1;
    size_t column_end = 0;
    // Gain a little bit speed by creating threads now.
    // if-clauses for non-square matrices
    #pragma omp parallel 
    // If height is bigger than height
    if(M >= N) {
        // Do until the first row (and its diagonal parts) has been computed
        for(unsigned int k = 1; k < (N+1); k++) {
            #pragma omp single
            column_beginning++;
            unsigned int i = row_beginning;
        
            // Scheduling has no positive effect :(
            #pragma omp for 
            for(unsigned int j = column_beginning; j > column_end; j--) {
                matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
            }  
        }
        // Do until the first column (and its diagonal parts) has been computed
        for(unsigned int k = N+1; k < (M+1); k++) {
            #pragma omp single
            row_beginning++;
            unsigned int i = row_beginning;
        
            // Scheduling has no positive effect :(
            #pragma omp for 
            for(unsigned int j = column_beginning; j > column_end; j--) {
                matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
            } 
        }
        // Do all the rest
        for(unsigned int k = M+1; k < (M+N); k++) {
            #pragma omp single
            {
                row_beginning++;
                column_end++;   
            }
            unsigned int i = row_beginning;
        
            // Scheduling has no positive effect :(
            #pragma omp for 
            for(unsigned int j = column_beginning; j > column_end; j--) {
                matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
            } 
        }
    } else {
        // Do until the first column (and its diagonal parts) has been computed
        for(unsigned int k = 1; k < (M+1); k++) {
            #pragma omp single
            column_beginning++;
            unsigned int i = row_beginning;
        
            // Scheduling has no positive effect :(
            #pragma omp for 
            for(unsigned int j = column_beginning; j > column_end; j--) {
                matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
            }  
        }
         // Do until the first row (and its diagonal parts) has been computed
        for(unsigned int k = M+1; k < (N+1); k++) {
            #pragma omp single 
            {
                column_beginning++;
                column_end++;   
            }
            unsigned int i = row_beginning;
        
            // Scheduling has no positive effect :(
            #pragma omp for 
            for(unsigned int j = column_beginning; j > column_end; j--) {
                matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
            } 
        }
        // Do all the rest
        for(unsigned int k = N+1; k < (M+N); k++) {
            #pragma omp single
            {
                row_beginning++;
                column_end++;
            }
            unsigned int i = row_beginning;
        
            // Scheduling has no positive effect :(
            #pragma omp for 
            for(unsigned int j = column_beginning; j > column_end; j--) {
                matrix[AT(i+column_beginning-j,j)] = (Q[i+column_beginning-j-1]-S[j-1])*(Q[i+column_beginning-j-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j-1)],
                              std::min<double>(matrix[AT(i+column_beginning-j-1, j+0)],
                                               matrix[AT(i+column_beginning-j+0, j-1)]));  
            } 
        }
    }
    
    return matrix[AT(M, N)];
}

double relax_C(double * Q, double * S, const size_t M, const size_t N) {
    
    double sum = 0.0;

    for (int i = 0; i < std::min<size_t>(M, N); i++)
        sum += (Q[i]-S[i])*(Q[i]-S[i]);

    return sum;
}

template <class F>
void benchmark(F f, double * Q, double * S, size_t M, size_t N, int i, int function) {

    auto start = std::chrono::system_clock::now();
   // std::cout << f(Q, S, M, N) << std::endl;
    f(Q, S, M, N);
    auto stop  = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = stop-start;
    time_elapsed[i+100*function] = elapsed_seconds.count();
    if(i%10 == 0)
        std::cout << elapsed_seconds.count() << " s\t";
}


int main() {

    size_t M = 1024, N = 1024;
    
    std::vector<double> Q(M);
    std::vector<double> S(N);
    std::iota(Q.begin(), Q.end(), 0);
    std::iota(S.begin(), S.end(), 0);
    std::for_each(Q.begin(), Q.end(), [](double& x){x=cos(0.031415*x);});
    std::for_each(S.begin(), S.end(), [](double& x){x=cos(2*0.031415*x);});

    // Just to see a success :D
    std::cout << "303.364\n251.799\n1002.28" << std::endl;
    std::cout << "# A\t\tB\t\tC\t\tD" << std::endl;
    for(unsigned int i = 0; i < ROUNDS; i++) {
        if(i%10 == 0)
            std::cout << "# ";
        benchmark(relax_A, Q.data(), S.data(), M, N, i, 0);
        benchmark(relax_B, Q.data(), S.data(), M, N, i, 1);
        benchmark(relax_C, Q.data(), S.data(), M, N, i, 2);
        benchmark(relax_D, Q.data(), S.data(), M, N, i, 3);
        if(i%10 == 0)
            std::cout << std::endl;        
    }
    double sums[4] = {0};
    double max[4] = {0};
    for(unsigned int i = 0; i < ROUNDS; i++) {
        sums[0] += time_elapsed[i];
        sums[1] += time_elapsed[i+100];
        sums[2] += time_elapsed[i+200];
        sums[3] += time_elapsed[i+300];        
    }
    
    std::cout << "#MeanA\t\tMeanB\t\tMeanC\t\tMeanD" << std::endl;
    std::cout << "# " << (sums[0]/ROUNDS) << " s\t" << (sums[1]/ROUNDS) << " s\t" << 
        (sums[2]/ROUNDS) << " s\t" << (sums[3]/ROUNDS) << " s\t" << std::endl;
}