#include <iostream>
#include <deque>
#include <cmath>
#include <chrono>
#include <fstream>

#define ROUNDS (1000)
double very_expensive_method(size_t node) {
    //std::cout << "# " << node << std::endl;
    double result = 0.0;
    
    for (int i = 1; i < node; i++)
        for (int j = 1; j < node; j++)
            result += (2*(i&1)-1)/pow(i+j, 2);
}

// Sequential version
void tree_bfs() {
    std::deque<size_t> queue = {0};
    while(queue.size()) {
        auto node = queue.front();
        queue.pop_front();             
        very_expensive_method(node);
        if (2*node+2 < (1<<10)) {
            queue.push_back(2*node+1);
            queue.push_back(2*node+2);
        } 
    }   
}

void tree_bfs_parallel() {
    std::deque<size_t> queue = {0};
    #pragma omp parallel 
    {
        #pragma omp single 
        {
            while(queue.size()) {
                auto node = queue.front();
                queue.pop_front();
                #pragma omp task   
                    {very_expensive_method(node);}
                if (2*node+2 < (1<<10)) {
                    queue.push_back(2*node+1);
                    queue.push_back(2*node+2);
                }
            } 
        } // End of single region
    } // End of parallel region   
}

// Sequential version in for loop
void tree_bfs_seq_for() {
    std::deque<size_t> queue = {0};

    for(int node = 0; node<(1<<10); node++){
        
        very_expensive_method(node);
    }
}

// Parallel version in for loop
void tree_bfs_parallel_for() {
    std::deque<size_t> queue = {0};
    #pragma omp parallel for
    for(int node = 0; node<(1<<10); node++){      
        very_expensive_method(node);
    }
}

// Parallel version in for loop
void tree_bfs_parallel_for_Tass() {
    std::deque<size_t> queue = {0};
    #pragma omp parallel 
    {
    for(int height = 0; height < 10; height++){
        #pragma omp single
        {
            for(int node = (1<<height); node<(1<<(height+1)); node++){
            #pragma omp task
                {very_expensive_method(node);
                   // std::cout << "# " << node << std::endl;
                }
            }
        }
    }
    }
}
    
int main() {
    std::ofstream task2Bench;
    task2Bench.open ("task2BenchPC", std::ios_base::app);
    task2Bench << "Sequential;Parallel;Sequential_For;Parallel_For;Tass\n";
    task2Bench.close();
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed_seconds;
    
    for (unsigned int i = 0; i < ROUNDS; i++) {
    
        start = std::chrono::system_clock::now();
        tree_bfs();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2BenchPC", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
              
        start = std::chrono::system_clock::now();
        tree_bfs_parallel();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2BenchPC", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
              
        start = std::chrono::system_clock::now();
        tree_bfs_seq_for();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2BenchPC", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
              
        start = std::chrono::system_clock::now();
        tree_bfs_parallel_for();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2BenchPC", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
        
        start = std::chrono::system_clock::now();
        tree_bfs_parallel_for_Tass();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2BenchPC", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";\n";
        task2Bench.close();
    }
        
    std::cout << "programme terminated" << std::endl;
} 
