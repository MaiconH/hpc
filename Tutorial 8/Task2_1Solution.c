#include <iostream>
#include <deque>
#include <cmath>
#include <chrono>

void very_expensive_method(size_t node) {
   // std::cout << "# " << node << std::endl;
    double result = 0.0;
    
    for (int i = 1; i < node; i++)
        for (int j = 1; j < node; j++)
            result += (2*(i&1)-1)/pow(i+j, 2);
}

// Sequential version in for loop
void tree_bfs_seq_for() {
    std::deque<size_t> queue = {0};

    for(int node = 0; node<(1<<10); node++){
        
        very_expensive_method(node);
    }
}

// Parallel version in for loop
void tree_bfs_parallel_for() {
    std::deque<size_t> queue = {0};
    #pragma omp parallel for
    for(int node = 0; node<(1<<10); node++){      
        very_expensive_method(node);
    }
}

int main() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    tree_bfs();
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    start = std::chrono::system_clock::now();
    tree_bfs_Parallel();
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    std::cout << "programme terminated" << std::endl;
} 
