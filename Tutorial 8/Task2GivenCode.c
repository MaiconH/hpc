#include <iostream>
#include <deque>

void very_expensive_method(size_t node) {
    std::cout << "# " << node << std::endl;
}

void tree_bfs() {
    std::deque<size_t> queue = {0};
    
    while(queue.size()) {
        auto node = queue.front();
        queue.pop_front();

        very_expensive_method(node);

        if (2*node+2 < (1<<10)) {
            queue.push_back(2*node+1);
            queue.push_back(2*node+2);
        }
    }   
}

int main() {
    tree_bfs();
    std::cout << "programme terminated" << std::endl;
} 
