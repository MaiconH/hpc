#include <iostream>
#include <deque>
#include <cmath>
#include <chrono>

double very_expensive_method(size_t node) {
    //std::cout << "# " << node << std::endl;
    double result = 0.0;
    
    for (int i = 1; i < node; i++)
        for (int j = 1; j < node; j++)
            result += (2*(i&1)-1)/pow(i+j, 2);
}

// Sequential version
void tree_bfs() {
    std::deque<size_t> queue = {0};
    while(queue.size()) {
        auto node = queue.front();
        queue.pop_front();             
        very_expensive_method(node);
        if (2*node+2 < (1<<10)) {
            queue.push_back(2*node+1);
            queue.push_back(2*node+2);
        } 
    }   
}

void tree_bfs_parallel() {
    std::deque<size_t> queue = {0};
    #pragma omp parallel 
    {
        #pragma omp single 
        {
            while(queue.size()) {
                auto node = queue.front();
                queue.pop_front();
                #pragma omp task   
                    {very_expensive_method(node);}
                if (2*node+2 < (1<<10)) {
                    queue.push_back(2*node+1);
                    queue.push_back(2*node+2);
                }
            } 
        } // End of single region
    } // End of parallel region   
}

// Sequential version in for loop
void tree_bfs_seq_for() {
    std::deque<size_t> queue = {0};

    for(int node = 0; node<(1<<10); node++){
        
        very_expensive_method(node);
    }
}

// Parallel version in for loop
void tree_bfs_parallel_for() {
    std::deque<size_t> queue = {0};
    #pragma omp parallel for
    for(int node = 0; node<(1<<10); node++){      
        very_expensive_method(node);
    }
}

int main() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    tree_bfs();
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    start = std::chrono::system_clock::now();
    tree_bfs_parallel();
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    start = std::chrono::system_clock::now();
    tree_bfs_seq_for();
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout << "# elapsed time (seq_for): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    start = std::chrono::system_clock::now();
    tree_bfs_parallel_for();
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel_for): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    std::cout << "programme terminated" << std::endl;
} 
