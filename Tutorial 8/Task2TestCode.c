#include <iostream>
#include <deque>

void very_expensive_method(size_t node) {
    std::cout << "# " << node << std::endl;
}

void tree_bfs() {
    std::deque<size_t> queue = {0};

    for(int node = 0; node<(1<<10); node++){
        
        very_expensive_method(node);
    }
}

int main() {
    tree_bfs();
    std::cout << "programme terminated" << std::endl;
} 
