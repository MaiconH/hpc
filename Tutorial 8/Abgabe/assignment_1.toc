\contentsline {section}{1 Parallelization of Elastic Measures with OpenMP (12P)}{3}
\contentsline {subsection}{(i) Visualize the data dependencies of both methods with the help of directed graphs. To achieve that, draw an exemplary matrix for M=N=6 and sketch the dependencies between the cells with arrows.}{4}
\contentsline {subsection}{(ii) Can you find an appropriate parallelization scheme for both provided methods? Which cells can be relaxed (updated) simultaneously?}{6}
\contentsline {subsection}{(iii) Implement an efficiently parallelized version of \texttt {relax\_A} using proper OpenMP pragmas. Also parallelize the initialization method \texttt {init\_matrix}.}{7}
\contentsline {subsection}{(iv) This task is optional: Implement a parallel version of \texttt {relax\_B} using OpenMP.}{14}
\contentsline {section}{2 Task Parallelism in OpenMP (8P)}{15}
\contentsline {subsection}{(i) Study the slides dealing with task parallelism in OpenMP.}{15}
\contentsline {subsection}{(ii) Implement a parallel version of \texttt {tree\_bfs} using OpenMP's for-pragmas by explicity enumerating the state space.}{16}
\contentsline {subsection}{(iii) Parallelize the method \texttt {tree\_bfs} using OpenMP's task mechanism. Do we need explicit synchronization barriers?}{18}
