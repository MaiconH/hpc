void tree_bfs() {
    std::deque<size_t> queue = {0};
    
    while(queue.size()) {
        auto node = queue.front();
        queue.pop_front();

        very_expensive_method(node);

        if (2*node+2 < (1<<10)) {
            queue.push_back(2*node+1);
            queue.push_back(2*node+2);
        }
    }   
}
