#define INFTY (std::numeric_limits<double>::infinity())
#define AT(i,j) ((i)*(N+1)+(j))

void init_matrix(double * matrix, const size_t M, const size_t N) {
     matrix[AT(0, 0)] = 0.0;
    for (size_t j = 1; j < N+1; j++)
        matrix[AT(0, j)] = INFTY;
    for (size_t i = 1; i < M+1; i++)
        matrix[AT(i, 0)] = INFTY;
}

double relax_A(double * Q, double * S, const size_t M, const size_t N) {
    std::vector<double> matrix((M+1)*(N+1));
    init_matrix(matrix.data(), M, N);

    for (size_t i = 1; i < M+1; i++) 
        for (size_t j = 1; j < N+1; j++) {
            double bsf = matrix[AT(i-1, j-1)];
            if (i > 1)
                bsf = std::min<double>(bsf, matrix[AT(i-2, j-1)]);
            if (j > 1)
                bsf = std::min<double>(bsf, matrix[AT(i-1, j-2)]);              
            matrix[AT(i,j)] = bsf + (Q[i-1]-S[j-1])*(Q[i-1]-S[j-1]);       
        }
    return matrix[AT(M, N)];
}

double relax_B(double * Q, double * S, const size_t M, const size_t N) {
    std::vector<double> matrix((M+1)*(N+1));
    init_matrix(matrix.data(), M, N);

    for (size_t i = 1; i < M+1; i++) 
        for (size_t j = 1; j < N+1; j++)
            matrix[AT(i,j)] = (Q[i-1]-S[j-1])*(Q[i-1]-S[j-1]) +
                              std::min<double>(matrix[AT(i-1, j-1)],
                              std::min<double>(matrix[AT(i-1, j+0)],
                                               matrix[AT(i+0, j-1)]));
    return matrix[AT(M, N)];
}