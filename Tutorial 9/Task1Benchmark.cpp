#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <immintrin.h>   // avx
#include <assert.h>
#include <chrono>
#include <omp.h> // Needed for omp_get_thread_num
#define NUM_THREADS (4)

#define RES (100)
#define ROUNDS (100)

/////////////////////////////Sequential////////////////////////////////////////////////
template <size_t N, class T>
void init_cloth(std::vector<T>& X, std::vector<T>& Y, std::vector<T>& Z, 
                std::vector<T>& U, std::vector<T>& V, std::vector<T>& W) {

    assert(N > 2);

    X = std::vector<T>(N*N);
    Y = std::vector<T>(N*N);
    Z = std::vector<T>(N*N, static_cast<T>(1));
    U = std::vector<T>(N*N, static_cast<T>(0));
    V = std::vector<T>(N*N, static_cast<T>(0));
    W = std::vector<T>(N*N, static_cast<T>(0));

    for (size_t i = 0; i < N; i++) {
        const T x = static_cast<T>(-2) + static_cast<T>(4*i)/(N-1);
        for (size_t j = 0; j < N; j++) {
            const T y = static_cast<T>(-2) + static_cast<T>(4*j)/(N-1);
            X[i*N+j] = x;
            Y[i*N+j] = y;
        }
    }
}

template <size_t N, bool dump=true, class T>
void calc_cloth(std::vector<T>& X, std::vector<T>& Y, std::vector<T>& Z, 
                std::vector<T>& U, std::vector<T>& V, std::vector<T>& W,
                T eps=1E-2, T bias=0.15, size_t steps=250, size_t iters=32) {

    assert(N > 2);

    // constraints and temporary memory
    const T cnstr_two = X[N]-X[0];
    const T cnstr_dia = sqrt(2*cnstr_two*cnstr_two);    
    std::vector<T> tmp_X(X.size()), tmp_Y(Y.size()), tmp_Z(Z.size());

    // integration step for physics
    auto update_positions = [&](T& x, T& y ,T& z, T& u, T& v, T& w){
        
        w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : static_cast<T>(0);
        x += eps*u;
        y += eps*v;
        z += eps*w;
    };

    auto adjust_positions = [&](T& x, T& y ,T& z) {

        T rho = x*x+y*y+z*z;
        if (rho < 1) {
            rho = sqrt(rho);
            x /= rho;
            y /= rho;
            z /= rho;
        }
        z = std::max<T>(z, -1);
    };

    // relaxation step for constraints
    auto relax_constraint = [&](size_t l, size_t m, T constraint){
        
        T delta_x = X[l]-X[m];
        T delta_y = Y[l]-Y[m];
        T delta_z = Z[l]-Z[m];

        T length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
        T displacement = (length-constraint)*bias;

        delta_x /=length;
        delta_y /=length;
        delta_z /=length;

        tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
        tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
        tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;
    };

    for (size_t step = 0; step < steps; step++) {
        
        // let gravitiy do its job
        for (size_t i = 0; i < N; i++)
            for (size_t j = 0; j < N; j++)
                update_positions(X[i*N+j], Y[i*N+j], Z[i*N+j],
                                 U[i*N+j], V[i*N+j], W[i*N+j]);

        // fix the constraints
        for (size_t iter = 0; iter < iters; iter++) {

            std::copy(X.begin(), X.end(), tmp_X.begin());
            std::copy(Y.begin(), Y.end(), tmp_Y.begin());
            std::copy(Z.begin(), Z.end(), tmp_Z.begin());

            for (size_t i = 0; i < N-1; i++)
                for (size_t j = 0; j < N; j++)
                    relax_constraint(i*N+j, (i+1)*N+j, cnstr_two);

            for (size_t i = 0; i < N-2; i++)
                for (size_t j = 0; j < N; j++)
                    relax_constraint(i*N+j, (i+2)*N+j, 2*cnstr_two);
            
            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N-1; j++)
                    relax_constraint(i*N+j, i*N+j+1, cnstr_two);

            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N-2; j++)
                    relax_constraint(i*N+j, i*N+j+2, 2*cnstr_two);

            for (size_t i = 0; i < N-1; i++)
                for (size_t j = 0; j < N-1; j++) 
                    relax_constraint(i*N+j, (i+1)*N+j+1, cnstr_dia);

            for (size_t i = 1; i < N; i++)
                for (size_t j = 0; j < N-1; j++) 
                    relax_constraint(i*N+j, (i-1)*N+j+1, cnstr_dia);            

            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N; j++)
                    adjust_positions(tmp_X[i*N+j], tmp_Y[i*N+j], tmp_Z[i*N+j]);

            std::copy(tmp_X.begin(), tmp_X.end(), X.begin());
            std::copy(tmp_Y.begin(), tmp_Y.end(), Y.begin());
            std::copy(tmp_Z.begin(), tmp_Z.end(), Z.begin());
        }

    }
}

void sequential() {

    std::vector<double> X, Y, Z, U, V, W;
    init_cloth<RES> (X, Y, Z, U, V, W);
    calc_cloth<RES> (X, Y, Z, U, V, W);
}

////////////////////////////////////////////////////////////////////////////////


/////////////////////////////AVX////////////////////////////////////////////////


void init_doubles(double* X, double* Y, double* Z, double* U, double* V, double* W) {
    int N = RES;
    for (size_t i = 0; i < N; i++) {
        double x = -2 + (4*i)/(N-1);
        for (size_t j = 0; j < N; j++) {
            double y = (-2) + (4*j)/(N-1);
            X[i*N+j] = x;
            Y[i*N+j] = y;
        }
    }
}

double relax_constraint_avx_col_AVX  (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z){
 
    __m256d xl = _mm256_load_pd(&X[l]);
    // Following line gives segmentation fault if AVX is used for the 
    // fifth constraint in the inner loop
    __m256d xm = _mm256_load_pd(&X[m]);
    __m256d yl = _mm256_load_pd(&Y[l]);
    __m256d ym = _mm256_load_pd(&Y[m]);
    __m256d zl = _mm256_load_pd(&Z[l]);
    __m256d zm = _mm256_load_pd(&Z[m]);
    
    __m128d bias_m128d = _mm_set_pd(bias, bias);
    __m256d bias_m256d = _mm256_castpd128_pd256(bias_m128d);
    bias_m256d = _mm256_insertf128_pd(bias_m256d, bias_m128d, 1);
    __m128d constraint_m128d = _mm_set_pd(constraint, constraint);
    __m256d constraint_m256d = _mm256_castpd128_pd256(constraint_m128d);
    constraint_m256d = _mm256_insertf128_pd(constraint_m256d, constraint_m128d, 1);
    
    __m256d delta_x = _mm256_sub_pd(xl, xm);
    __m256d delta_y = _mm256_sub_pd(yl, ym);
    __m256d delta_z = _mm256_sub_pd(zl, zm);
    
    __m256d length = _mm256_add_pd(_mm256_mul_pd(delta_x,delta_x),_mm256_mul_pd(delta_y,delta_y));
    length = _mm256_add_pd(_mm256_mul_pd(delta_z, delta_z), length); 
    __m256d displacement = _mm256_mul_pd(bias_m256d, _mm256_sub_pd(length, constraint_m256d));

    delta_x = _mm256_div_pd(delta_x, length);
    delta_y = _mm256_div_pd(delta_y, length);
    delta_z = _mm256_div_pd(delta_z, length);

    _mm256_store_pd(&tmp_X[l], _mm256_sub_pd(xl, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp_X[m], _mm256_add_pd(xm, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp_Y[l], _mm256_sub_pd(yl, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp_Y[m], _mm256_add_pd(ym, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp_Z[l], _mm256_sub_pd(zl, _mm256_mul_pd(delta_z, displacement)));
    _mm256_store_pd(&tmp_Z[m], _mm256_add_pd(zm, _mm256_mul_pd(delta_z, displacement)));
}

double relax_constraint_avx_row_AVX  (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z, int N, 
                            double* tmp2_X, double* tmp2_Y, double* tmp2_Z){
 
 //   std::cout << "Before Xl" << std::endl;
    __m128d xl_m128d = _mm_set_pd(X[l], X[l+N]);
    __m128d xl_m128d_high = _mm_set_pd(X[l+2*N], X[l+3*N]);
    __m256d xl_m256d = _mm256_castpd128_pd256(xl_m128d);
    xl_m256d = _mm256_insertf128_pd(xl_m256d, xl_m128d_high, 1);
    
  //  std::cout << "Before Xm" << std::endl;
    __m128d xm_m128d = _mm_set_pd(X[m], X[m+N]);
    __m128d xm_m128d_high = _mm_set_pd(X[m+2*N], X[m+3*N]);
    __m256d xm_m256d = _mm256_castpd128_pd256(xm_m128d);
    xm_m256d = _mm256_insertf128_pd(xm_m256d, xm_m128d_high, 1);

  //  std::cout << "Before Yl" << std::endl;
    __m128d yl_m128d = _mm_set_pd(Y[l], Y[l+N]);
    __m128d yl_m128d_high = _mm_set_pd(Y[l+2*N], Y[l+3*N]);
    __m256d yl_m256d = _mm256_castpd128_pd256(yl_m128d);
    yl_m256d = _mm256_insertf128_pd(yl_m256d, yl_m128d_high, 1);
    
   // std::cout << "Before Ym" << std::endl;
    __m128d ym_m128d = _mm_set_pd(Y[m], Y[m+N]);
    __m128d ym_m128d_high = _mm_set_pd(Y[m+2*N], Y[m+3*N]);
    __m256d ym_m256d = _mm256_castpd128_pd256(ym_m128d);
    ym_m256d = _mm256_insertf128_pd(ym_m256d, ym_m128d_high, 1);
    
 //   std::cout << "Before Zl" << std::endl;
    __m128d zl_m128d = _mm_set_pd(Z[l], Z[l+N]);
    __m128d zl_m128d_high = _mm_set_pd(Z[l+2*N], Z[l+3*N]);
    __m256d zl_m256d = _mm256_castpd128_pd256(zl_m128d);
    zl_m256d = _mm256_insertf128_pd(zl_m256d, zl_m128d_high, 1); 
    
//  std::cout << "Before Zm" << std::endl;
    __m128d zm_m128d = _mm_set_pd(Z[m], Z[m+N]);
    __m128d zm_m128d_high = _mm_set_pd(Z[m+2*N], Z[m+3*N]);
    __m256d zm_m256d = _mm256_castpd128_pd256(zm_m128d);
    zm_m256d = _mm256_insertf128_pd(zm_m256d, zm_m128d_high, 1);
    
  // std::cout << "Before bias" << std::endl;
    __m128d bias_m128d = _mm_set_pd(bias, bias);
    __m256d bias_m256d = _mm256_castpd128_pd256(bias_m128d);
    bias_m256d = _mm256_insertf128_pd(bias_m256d, bias_m128d, 1);
 //  std::cout << "Before constraint" << std::endl;
    __m128d constraint_m128d = _mm_set_pd(constraint, constraint);
    __m256d constraint_m256d = _mm256_castpd128_pd256(constraint_m128d);
    constraint_m256d = _mm256_insertf128_pd(constraint_m256d, constraint_m128d, 1);
    
 //   std::cout << "Before delta" << std::endl;
    __m256d delta_x = _mm256_sub_pd(xl_m256d, xm_m256d);
    __m256d delta_y = _mm256_sub_pd(yl_m256d, ym_m256d);
    __m256d delta_z = _mm256_sub_pd(zl_m256d, zm_m256d);
    
  //  std::cout << "Before length" << std::endl;
    __m256d length = _mm256_add_pd(_mm256_mul_pd(delta_x,delta_x),_mm256_mul_pd(delta_y,delta_y));
    length = _mm256_add_pd(_mm256_mul_pd(delta_z, delta_z), length); 
    __m256d displacement = _mm256_mul_pd(bias_m256d, _mm256_sub_pd(length, constraint_m256d));

  //  std::cout << "Before delta/length" << std::endl;
    delta_x = _mm256_div_pd(delta_x, length);
    delta_y = _mm256_div_pd(delta_y, length);
    delta_z = _mm256_div_pd(delta_z, length);

  //  std::cout << "Before store" << std::endl;
    _mm256_store_pd(&tmp2_X[0], _mm256_sub_pd(xl_m256d, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp2_X[4], _mm256_add_pd(xm_m256d, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp2_Y[0], _mm256_sub_pd(yl_m256d, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp2_Y[4], _mm256_add_pd(ym_m256d, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp2_Z[0], _mm256_sub_pd(zl_m256d, _mm256_mul_pd(delta_z, displacement)));
    _mm256_store_pd(&tmp2_Z[4], _mm256_add_pd(zm_m256d, _mm256_mul_pd(delta_z, displacement)));
    
 //   std::cout << "Before for" << std::endl;
    for(int i = 0; i < 4; i++) {
        tmp_X[l+i*N] = tmp2_X[i];
        tmp_X[m+i*N] = tmp2_X[i+4];
        tmp_Y[l+i*N] = tmp2_Y[i];
        tmp_Y[m+i*N] = tmp2_Y[i+4];
        tmp_Z[l+i*N] = tmp2_Z[i];
        tmp_Z[m+i*N] = tmp2_Z[i+4];
    }

   // std::cout << "End" << std::endl;
}

double relax_constraint_normal_AVX  (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z){

    double delta_x = X[l]-X[m];
    double delta_y = Y[l]-Y[m];
    double delta_z = Z[l]-Z[m];
    
    double length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
    double displacement = (length-constraint)*bias;

    delta_x /=length;
    delta_y /=length;
    delta_z /=length;

    tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
    tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
    tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;

}


double update_positions_AVX  (double& x, double& y ,double& z, double& u, double& v, double& w, double eps){
    
    w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : (0);
    x += eps*u;
    y += eps*v;
    z += eps*w;
}

void adjust_positions_AVX (double& x, double& y ,double& z) {

    double rho = x*x+y*y+z*z;
    if (rho < 1) {
        rho = sqrt(rho);
        x /= rho;
        y /= rho;
        z /= rho;
    }
    if(z < -1)
        z = -1;    
}

void testDouble_AVX(double* X, double* Y, double* Z, 
                double* U, double* V, double* W, int N,
                double eps=1E-2, double bias=0.15, size_t steps=250, size_t iters=32) {
 
    std::ofstream ofile("dataAVXCompleteVersion2.bin", std::ios::binary);
    // constraints and temporary memory
    double cnstr_two = X[N]-X[0];
    double cnstr_dia = sqrt(2*cnstr_two*cnstr_two);
    double * tmp_X = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * tmp_Y = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * tmp_Z = (double *)_mm_malloc(RES*RES*sizeof(double), 64);

    //l begins with 0 and m begins with 4
    double * tmp2_X = (double *)_mm_malloc(4*2*sizeof(double), 64);
    double * tmp2_Y = (double *)_mm_malloc(4*2*sizeof(double), 64);
    double * tmp2_Z = (double *)_mm_malloc(4*2*sizeof(double), 64);

    for (size_t step = 0; step < steps; step++) {
       // std::cout << "\n\nStep " << step <<  ", Iter: " << std::endl;
        // Para: Whole Mesh; Example pragma omp for twice
        // Stepwise dependency
        // let gravitiy do its job
        for (size_t i = 0; i < N; i++)
            for (size_t j = 0; j < N; j++) {
                
                update_positions_AVX(X[i*N+j], Y[i*N+j], Z[i*N+j],
                                 U[i*N+j], V[i*N+j], W[i*N+j], eps);
            }

        // fix the constraints
        for (size_t iter = 0; iter < iters; iter++) {
           // std::cout << iter << ", ";
            std::copy(&X[0], &X[N*N], &tmp_X[0]);
            std::copy(&Y[0], &Y[N*N], &tmp_Y[0]);
            std::copy(&Z[0], &Z[N*N], &tmp_Z[0]);
    
            // Check constraints with mass point down (vertical)
            for (size_t i = 0; i < N-1; i++)
                // Can be parallelized (since each column is independent 
                // from each other.
                // AVX 256 bit = 4 double precision floats
                for (size_t j = 0; j < N; j+=4)
                    relax_constraint_avx_col_AVX(i*N+j, (i+1)*N+j, cnstr_two, X, Y, Z, bias, eps, 
                        tmp_X, tmp_Y, tmp_Z);
                
            // Parallelize two i's (rows) at once (row is independent from the
            // direct neighbor. Two threads are possible.
            // Check constraints green vertical
            for (size_t i = 0; i < N-2; i++)
                // Can be parallelized
                for (size_t j = 0; j < N; j+=4)
                    relax_constraint_avx_col_AVX(i*N+j, (i+2)*N+j, 2*cnstr_two, X, Y, Z, bias, eps, 
                        tmp_X, tmp_Y, tmp_Z);

                   
                    
            // Check constraints blue to the right
            // Can be parallelized since each row is independent.
            for (size_t i = 0; i < N; i+=4)
                for (size_t j = 0; j < N-1; j++)
                   // relax_constraint_normal_AVX(i*N+j, i*N+j+1, cnstr_two, X, Y, Z, bias, eps, 
                    //    tmp_X, tmp_Y, tmp_Z);
                      relax_constraint_avx_row_AVX(i*N+j, i*N+j+1, cnstr_two, X, Y, Z, bias, eps, 
                        tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);

            // Can be parallelized since each row is independent.
            // Check constraints green to the right
            for (size_t i = 0; i < N; i+=4)
                // With 2 threads you can compute two columns at once.
                for (size_t j = 0; j < N-2; j++)
                    relax_constraint_avx_row_AVX(i*N+j, i*N+j+2, 2*cnstr_two, X, Y, Z, bias, eps, 
                        tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);
            
            // Can be parallelized row-wise or column-wise <- cannot use AVX
            // Check constraints magenta right down
            for (size_t i = 0; i < N-1; i+=4) 
                for (size_t j = 0; j < N-1; j++) 
                    relax_constraint_avx_row_AVX(i*N+j, (i+1)*N+j+1, cnstr_dia, X, Y, Z, bias, eps, 
                        tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);

         //   std::cout << "Before" << std::endl;
            // Can be parallelized row-wise
            // Check constraints magenta right up
            for (size_t i = 1; i < N; i+=4) {
            //    std::cout << "\ni " << i << std::endl;
                for (size_t j = 0; j < N-1; j++) {
                   // std::cout << j << ", " << std::endl;
               //     relax_constraint_normal_AVX(i*N+j, (i-1)*N+j+1, cnstr_dia, X, Y, Z, bias, eps, 
               //         tmp_X, tmp_Y, tmp_Z); 
                    relax_constraint_avx_row_AVX(i*N+j, (i-1)*N+j+1, cnstr_dia, X, Y, Z, bias, eps, 
                        tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);         
                }
            }

          //  std::cout << "After" << std::endl;
                    
            // Both loops can be parallelized
            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N; j++)
                    adjust_positions_AVX(tmp_X[i*N+j], tmp_Y[i*N+j], tmp_Z[i*N+j]);

            std::copy(&tmp_X[0], &tmp_X[N*N], &X[0]);
            std::copy(&tmp_Y[0], &tmp_Y[N*N], &Y[0]);
            std::copy(&tmp_Z[0], &tmp_Z[N*N], &Z[0]);
                    
        }
         
        if (true) {
            // make sure you only do this once XD
            ofile.write((char*) X, sizeof(double)*N*N);
            ofile.write((char*) Y, sizeof(double)*N*N);
            ofile.write((char*) Z, sizeof(double)*N*N);
        }
        
    }
}

void avx() {
    double * X = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * Y = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * Z = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * U = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * V = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * W = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    init_doubles(X, Y, Z, U, V, W);
    testDouble_AVX(X, Y, Z, U, V, W, RES);
    
}
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////OpenMP///////////////////////////////////////

template <size_t N, bool dump=true, class T>
void calc_cloth_openmp(std::vector<T>& X, std::vector<T>& Y, std::vector<T>& Z, 
                std::vector<T>& U, std::vector<T>& V, std::vector<T>& W,
                T eps=1E-2, T bias=0.15, size_t steps=250, size_t iters=32) {

    assert(N > 2);

  //  std::ofstream ofile("data.bin", std::ios::binary);
    
    // constraints and temporary memory
    const T cnstr_two = X[N]-X[0];
    const T cnstr_dia = sqrt(2*cnstr_two*cnstr_two);    
    std::vector<T> tmp_X(X.size()), tmp_Y(Y.size()), tmp_Z(Z.size());

    // integration step for physics
    auto update_positions_openmp = [&](T& x, T& y ,T& z, T& u, T& v, T& w){
        
        w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : static_cast<T>(0);
        x += eps*u;
        y += eps*v;
        z += eps*w;
    };

    auto adjust_positions_openmp = [&](T& x, T& y ,T& z) {

        T rho = x*x+y*y+z*z;
        if (rho < 1) {
            rho = sqrt(rho);
            x /= rho;
            y /= rho;
            z /= rho;
        }
        z = std::max<T>(z, -1);
    };

    // Para: x, y and z
    // relaxation step for constraints
    auto relax_constraint_openmp = [&](size_t l, size_t m, T constraint){
        
        T delta_x = X[l]-X[m];
        T delta_y = Y[l]-Y[m];
        T delta_z = Z[l]-Z[m];

        T length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
        T displacement = (length-constraint)*bias;

        delta_x /=length;
        delta_y /=length;
        delta_z /=length;

        tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
        tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
        tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;
    };

    
    for (size_t step = 0; step < steps; step++) {
        #pragma omp parallel 
        {
            // Para: Whole Mesh; Example pragma omp for twice
            // Stepwise dependency
            // let gravitiy do its job
            #pragma omp for
            for (size_t i = 0; i < N; i++)
                //#pragma omp parallel for
                for (size_t j = 0; j < N; j++)
                    update_positions_openmp(X[i*N+j], Y[i*N+j], Z[i*N+j],
                                        U[i*N+j], V[i*N+j], W[i*N+j]);

            // fix the constraints
            for (size_t iter = 0; iter < iters; iter++) {

                #pragma omp single
                {
                    std::copy(X.begin(), X.end(), tmp_X.begin());         
                    std::copy(Y.begin(), Y.end(), tmp_Y.begin());
                    std::copy(Z.begin(), Z.end(), tmp_Z.begin());
                }
                // Check constraints with mass point down (vertical)
                for (size_t i = 0; i < N-1; i++)
                    // Can be parallelized (since each column is independent 
                    // from each other.
                    #pragma omp for
                    for (size_t j = 0; j < N; j++)
                        relax_constraint_openmp(i*N+j, (i+1)*N+j, cnstr_two);

                // Parallelize two i's (rows) at once (row is independent from the
                // direct neighbor. Two threads are possible.
                // Check constraints green vertical
                for (size_t i = 0; i < N-2; i++)
                    // Can be parallelized
                    #pragma omp for
                    for (size_t j = 0; j < N; j++)
                        relax_constraint_openmp(i*N+j, (i+2)*N+j, 2*cnstr_two);
                
                // Check constraints blue to the right
                // Can be parallelized since each row is independent.
                #pragma omp for
                for (size_t i = 0; i < N; i++)
                    for (size_t j = 0; j < N-1; j++)
                        relax_constraint_openmp(i*N+j, i*N+j+1, cnstr_two);

                // Can be parallelized since each row is independent.
                // Check constraints green to the right
                #pragma omp for
                for (size_t i = 0; i < N; i++)
                    // With 2 threads you can compute two columns at once.
                    for (size_t j = 0; j < N-2; j++)
                        relax_constraint_openmp(i*N+j, i*N+j+2, 2*cnstr_two);

                // Can be parallelized row-wise or column-wise
                // Check constraints magenta right down
                #pragma omp for
                for (size_t i = 0; i < N-1; i++)
                    for (size_t j = 0; j < N-1; j++) 
                        relax_constraint_openmp(i*N+j, (i+1)*N+j+1, cnstr_dia);

                // Can be parallelized row-wise
                // Check constraints magenta right up
                #pragma omp for
                for (size_t i = 1; i < N; i++)
                    for (size_t j = 0; j < N-1; j++) 
                        relax_constraint_openmp(i*N+j, (i-1)*N+j+1, cnstr_dia);            

                // Both loops can be parallelized
                #pragma omp for
                for (size_t i = 0; i < N; i++)
                    //#pragma omp parallel for
                    for (size_t j = 0; j < N; j++)
                        adjust_positions_openmp(tmp_X[i*N+j], tmp_Y[i*N+j], tmp_Z[i*N+j]);

                #pragma omp single
                {
                    std::copy(tmp_X.begin(), tmp_X.end(), X.begin());
                    std::copy(tmp_Y.begin(), tmp_Y.end(), Y.begin());
                    std::copy(tmp_Z.begin(), tmp_Z.end(), Z.begin());
                }
            } // End of iter-loop
        } // End of parallel region
/*
        if (dump) {
            // make sure you only do this once XD
            ofile.write((char*) X.data(), sizeof(T)*X.size());
            ofile.write((char*) Y.data(), sizeof(T)*Y.size());
            ofile.write((char*) Z.data(), sizeof(T)*Z.size());
        }
*/    }
}



void openmp() {
    std::vector<double> X, Y, Z, U, V, W;
    init_cloth<RES> (X, Y, Z, U, V, W);
    calc_cloth_openmp<RES> (X, Y, Z, U, V, W);
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////OpenMPAVX///////////////////////////////////////


double relax_constraint_avx_col_openmpavx  (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z){
 
    __m256d xl = _mm256_load_pd(&X[l]);
    __m256d xm = _mm256_load_pd(&X[m]);
    __m256d yl = _mm256_load_pd(&Y[l]);
    __m256d ym = _mm256_load_pd(&Y[m]);
    __m256d zl = _mm256_load_pd(&Z[l]);
    __m256d zm = _mm256_load_pd(&Z[m]);
    
    __m128d bias_m128d = _mm_set_pd(bias, bias);
    __m256d bias_m256d = _mm256_castpd128_pd256(bias_m128d);
    bias_m256d = _mm256_insertf128_pd(bias_m256d, bias_m128d, 1);
    __m128d constraint_m128d = _mm_set_pd(constraint, constraint);
    __m256d constraint_m256d = _mm256_castpd128_pd256(constraint_m128d);
    constraint_m256d = _mm256_insertf128_pd(constraint_m256d, constraint_m128d, 1);
    
    __m256d delta_x = _mm256_sub_pd(xl, xm);
    __m256d delta_y = _mm256_sub_pd(yl, ym);
    __m256d delta_z = _mm256_sub_pd(zl, zm);
    
    __m256d length = _mm256_add_pd(_mm256_mul_pd(delta_x,delta_x),_mm256_mul_pd(delta_y,delta_y));
    length = _mm256_add_pd(_mm256_mul_pd(delta_z, delta_z), length); 
    __m256d displacement = _mm256_mul_pd(bias_m256d, _mm256_sub_pd(length, constraint_m256d));

    delta_x = _mm256_div_pd(delta_x, length);
    delta_y = _mm256_div_pd(delta_y, length);
    delta_z = _mm256_div_pd(delta_z, length);

    _mm256_store_pd(&tmp_X[l], _mm256_sub_pd(xl, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp_X[m], _mm256_add_pd(xm, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp_Y[l], _mm256_sub_pd(yl, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp_Y[m], _mm256_add_pd(ym, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp_Z[l], _mm256_sub_pd(zl, _mm256_mul_pd(delta_z, displacement)));
    _mm256_store_pd(&tmp_Z[m], _mm256_add_pd(zm, _mm256_mul_pd(delta_z, displacement)));
}

double relax_constraint_avx_row_openmpavx  (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z, int N, 
                            double* tmp2_X, double* tmp2_Y, double* tmp2_Z){
 
    __m128d xl_m128d = _mm_set_pd(X[l], X[l+N]);
    __m128d xl_m128d_high = _mm_set_pd(X[l+2*N], X[l+3*N]);
    __m256d xl_m256d = _mm256_castpd128_pd256(xl_m128d);
    xl_m256d = _mm256_insertf128_pd(xl_m256d, xl_m128d_high, 1);
    
    __m128d xm_m128d = _mm_set_pd(X[m], X[m+N]);
    __m128d xm_m128d_high = _mm_set_pd(X[m+2*N], X[m+3*N]);
    __m256d xm_m256d = _mm256_castpd128_pd256(xm_m128d);
    xm_m256d = _mm256_insertf128_pd(xm_m256d, xm_m128d_high, 1);

    __m128d yl_m128d = _mm_set_pd(Y[l], Y[l+N]);
    __m128d yl_m128d_high = _mm_set_pd(Y[l+2*N], Y[l+3*N]);
    __m256d yl_m256d = _mm256_castpd128_pd256(yl_m128d);
    yl_m256d = _mm256_insertf128_pd(yl_m256d, yl_m128d_high, 1);
    
    __m128d ym_m128d = _mm_set_pd(Y[m], Y[m+N]);
    __m128d ym_m128d_high = _mm_set_pd(Y[m+2*N], Y[m+3*N]);
    __m256d ym_m256d = _mm256_castpd128_pd256(ym_m128d);
    ym_m256d = _mm256_insertf128_pd(ym_m256d, ym_m128d_high, 1);
    
    __m128d zl_m128d = _mm_set_pd(Z[l], Z[l+N]);
    __m128d zl_m128d_high = _mm_set_pd(Z[l+2*N], Z[l+3*N]);
    __m256d zl_m256d = _mm256_castpd128_pd256(zl_m128d);
    zl_m256d = _mm256_insertf128_pd(zl_m256d, zl_m128d_high, 1); 
    
    __m128d zm_m128d = _mm_set_pd(Z[m], Z[m+N]);
    __m128d zm_m128d_high = _mm_set_pd(Z[m+2*N], Z[m+3*N]);
    __m256d zm_m256d = _mm256_castpd128_pd256(zm_m128d);
    zm_m256d = _mm256_insertf128_pd(zm_m256d, zm_m128d_high, 1);
    
    __m128d bias_m128d = _mm_set_pd(bias, bias);
    __m256d bias_m256d = _mm256_castpd128_pd256(bias_m128d);
    bias_m256d = _mm256_insertf128_pd(bias_m256d, bias_m128d, 1);

    __m128d constraint_m128d = _mm_set_pd(constraint, constraint);
    __m256d constraint_m256d = _mm256_castpd128_pd256(constraint_m128d);
    constraint_m256d = _mm256_insertf128_pd(constraint_m256d, constraint_m128d, 1);
    
    __m256d delta_x = _mm256_sub_pd(xl_m256d, xm_m256d);
    __m256d delta_y = _mm256_sub_pd(yl_m256d, ym_m256d);
    __m256d delta_z = _mm256_sub_pd(zl_m256d, zm_m256d);
    
    __m256d length = _mm256_add_pd(_mm256_mul_pd(delta_x,delta_x),_mm256_mul_pd(delta_y,delta_y));
    length = _mm256_add_pd(_mm256_mul_pd(delta_z, delta_z), length); 
    __m256d displacement = _mm256_mul_pd(bias_m256d, _mm256_sub_pd(length, constraint_m256d));

    delta_x = _mm256_div_pd(delta_x, length);
    delta_y = _mm256_div_pd(delta_y, length);
    delta_z = _mm256_div_pd(delta_z, length);

    int shift_index = (4+NUM_THREADS)*omp_get_thread_num();

    _mm256_store_pd(&tmp2_X[0+shift_index], _mm256_sub_pd(xl_m256d, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp2_X[4+shift_index], _mm256_add_pd(xm_m256d, _mm256_mul_pd(delta_x, displacement)));
    _mm256_store_pd(&tmp2_Y[0+shift_index], _mm256_sub_pd(yl_m256d, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp2_Y[4+shift_index], _mm256_add_pd(ym_m256d, _mm256_mul_pd(delta_y, displacement)));
    _mm256_store_pd(&tmp2_Z[0+shift_index], _mm256_sub_pd(zl_m256d, _mm256_mul_pd(delta_z, displacement)));
    _mm256_store_pd(&tmp2_Z[4+shift_index], _mm256_add_pd(zm_m256d, _mm256_mul_pd(delta_z, displacement)));
    
    for(int i = 0; i < 4; i++) {
        tmp_X[l+i*N] = tmp2_X[i+shift_index];
        tmp_X[m+i*N] = tmp2_X[i+4+shift_index];
        tmp_Y[l+i*N] = tmp2_Y[i+shift_index];
        tmp_Y[m+i*N] = tmp2_Y[i+4+shift_index];
        tmp_Z[l+i*N] = tmp2_Z[i+shift_index];
        tmp_Z[m+i*N] = tmp2_Z[i+4+shift_index];
    }
}

double relax_constraint_normal_openmpavx  (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z){

    double delta_x = X[l]-X[m];
    double delta_y = Y[l]-Y[m];
    double delta_z = Z[l]-Z[m];
    
    double length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
    double displacement = (length-constraint)*bias;

    delta_x /=length;
    delta_y /=length;
    delta_z /=length;

    tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
    tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
    tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;
}


double update_positions_openmpavx  (double& x, double& y ,double& z, double& u, double& v, double& w, double eps){
    
    w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : (0);
    x += eps*u;
    y += eps*v;
    z += eps*w;
}

void adjust_positions_openmpavx (double& x, double& y ,double& z) {

    double rho = x*x+y*y+z*z;
    if (rho < 1) {
        rho = sqrt(rho);
        x /= rho;
        y /= rho;
        z /= rho;
    }
    if(z < -1)
        z = -1;    
}

void calc_falling(double* X, double* Y, double* Z, 
                double* U, double* V, double* W, int N,
                double eps=1E-2, double bias=0.15, size_t steps=250, size_t iters=32) {
 
    // std::ofstream ofile("dataAVXOpenMPComplete.bin", std::ios::binary);
    double cnstr_two = X[N]-X[0];
    double cnstr_dia = sqrt(2*cnstr_two*cnstr_two);
    double * tmp_X = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * tmp_Y = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * tmp_Z = (double *)_mm_malloc(RES*RES*sizeof(double), 64);

    //l begins with 0 and m begins with 4. Each thread has its own block of
    // values.
    double * tmp2_X = (double *)_mm_malloc(NUM_THREADS*4*2*sizeof(double), 64);
    double * tmp2_Y = (double *)_mm_malloc(NUM_THREADS*4*2*sizeof(double), 64);
    double * tmp2_Z = (double *)_mm_malloc(NUM_THREADS*4*2*sizeof(double), 64);
    

    for (size_t step = 0; step < steps; step++) {
        #pragma omp parallel num_threads(NUM_THREADS)
        {
            // Stepwise dependency
            // let gravitiy do its job
            #pragma omp for
            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N; j++)              
                    update_positions_openmpavx(X[i*N+j], Y[i*N+j], Z[i*N+j],
                                    U[i*N+j], V[i*N+j], W[i*N+j], eps);
                
            for (size_t iter = 0; iter < iters; iter++) {
                #pragma omp single
                {
                    std::copy(&X[0], &X[N*N], &tmp_X[0]);
                    std::copy(&Y[0], &Y[N*N], &tmp_Y[0]);
                    std::copy(&Z[0], &Z[N*N], &tmp_Z[0]);
                }
        
                // Check constraints with mass point down (vertical)
                for (size_t i = 0; i < N-1; i++)
                    // Can be parallelized (since each column is independent 
                    // from each other.
                    // AVX 256 bit = 4 double precision floats
                    #pragma omp for schedule(static, 1)
                    for (size_t j = 0; j < N; j+=4)
                        relax_constraint_avx_col_openmpavx(i*N+j, (i+1)*N+j, cnstr_two, X, Y, Z, bias, eps, 
                            tmp_X, tmp_Y, tmp_Z);
                    
                // Check constraints green vertical
                for (size_t i = 0; i < N-2; i++)
                    // Can be parallelized
                    #pragma omp for schedule(static, 1)
                    for (size_t j = 0; j < N; j+=4)
                        relax_constraint_avx_col_openmpavx(i*N+j, (i+2)*N+j, 2*cnstr_two, X, Y, Z, bias, eps, 
                            tmp_X, tmp_Y, tmp_Z);
                        
                // Check constraints blue to the right
                // Can be parallelized since each row is independent.
                #pragma omp for schedule(static, 1)
                for (size_t i = 0; i < N; i+=4)
                    for (size_t j = 0; j < N-1; j++)
                        relax_constraint_avx_row_openmpavx(i*N+j, i*N+j+1, cnstr_two, X, Y, Z, bias, eps, 
                            tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);

                // Can be parallelized since each row is independent.
                // Check constraints green to the right
                #pragma omp for schedule(static, 1)
                for (size_t i = 0; i < N; i+=4)
                    // With 2 threads you can compute two columns at once.
                    for (size_t j = 0; j < N-2; j++)
                        relax_constraint_avx_row_openmpavx(i*N+j, i*N+j+2, 2*cnstr_two, X, Y, Z, bias, eps, 
                            tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);
                
                // Can be parallelized row-wise or column-wise <- cannot use AVX
                // Check constraints magenta right down
                #pragma omp for schedule(static, 1)
                for (size_t i = 0; i < N-1; i+=4)  
                    for (size_t j = 0; j < N-N+1; j++) 
                        relax_constraint_avx_row_openmpavx(i*N+j, (i+1)*N+j+1, cnstr_dia, X, Y, Z, bias, eps, 
                            tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);

                // Can be parallelized row-wise
                // Check constraints magenta right up
                #pragma omp for schedule(static, 1)
                for (size_t i = 1; i < N; i+=4) 
                    for (size_t j = 0; j < N-1; j++) 
                        relax_constraint_avx_row_openmpavx(i*N+j, (i-1)*N+j+1, cnstr_dia, X, Y, Z, bias, eps, 
                            tmp_X, tmp_Y, tmp_Z, N, tmp2_X, tmp2_Y, tmp2_Z);     
 
                // Both loops can be parallelized
                #pragma omp for schedule(static, 1)
                for (size_t i = 0; i < N; i++)
                    for (size_t j = 0; j < N; j++)
                        adjust_positions_openmpavx(tmp_X[i*N+j], tmp_Y[i*N+j], tmp_Z[i*N+j]);

                #pragma omp single
                {
                    std::copy(&tmp_X[0], &tmp_X[N*N], &X[0]);
                    std::copy(&tmp_Y[0], &tmp_Y[N*N], &Y[0]);
                    std::copy(&tmp_Z[0], &tmp_Z[N*N], &Z[0]);
                }    
            }
        } // End of parallel region
    }
}

void openmpAVX() {
    double * X = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * Y = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * Z = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * U = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * V = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    double * W = (double *)_mm_malloc(RES*RES*sizeof(double), 64);
    init_doubles(X, Y, Z, U, V, W);
    calc_falling(X, Y, Z, U, V, W, RES);
}
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char const *argv[]) {
    
    std::ofstream task1Bench;
    task1Bench.open ("task1BenchLaptop2", std::ios_base::app);
    task1Bench << "Sequential;AVX;OpenMP;AVX with OpenMP\n";
    task1Bench.close();
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed_seconds;

    for (int i = 0; i < ROUNDS; i++) {
        std::cout << "Round " << i << std::endl;
        start = std::chrono::system_clock::now();
        sequential();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task1Bench.open ("task1BenchLaptop2", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";";
        task1Bench.close();

        start = std::chrono::system_clock::now();
        avx();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task1Bench.open ("task1BenchLaptop2", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";";
        task1Bench.close();

        start = std::chrono::system_clock::now();
        openmp();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task1Bench.open ("task1BenchLaptop2", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";";
        task1Bench.close();

        start = std::chrono::system_clock::now();
        openmpAVX();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task1Bench.open ("task1BenchLaptop2", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";\n";
        task1Bench.close();
    }
    
    std::cout << "programme terminated" << std::endl;
    return 0;
}