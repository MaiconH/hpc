#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>

#include <assert.h>

#define RES (100)

template <size_t N, class T>
void init_cloth(std::vector<T>& X, std::vector<T>& Y, std::vector<T>& Z, 
                std::vector<T>& U, std::vector<T>& V, std::vector<T>& W) {

    assert(N > 2);

    X = std::vector<T>(N*N);
    Y = std::vector<T>(N*N);
    Z = std::vector<T>(N*N, static_cast<T>(1));
    U = std::vector<T>(N*N, static_cast<T>(0));
    V = std::vector<T>(N*N, static_cast<T>(0));
    W = std::vector<T>(N*N, static_cast<T>(0));

    for (size_t i = 0; i < N; i++) {
        const T x = static_cast<T>(-2) + static_cast<T>(4*i)/(N-1);
        for (size_t j = 0; j < N; j++) {
            const T y = static_cast<T>(-2) + static_cast<T>(4*j)/(N-1);
            X[i*N+j] = x;
            Y[i*N+j] = y;
        }
    }
}

template <size_t N, bool dump=true, class T>
void calc_cloth(std::vector<T>& X, std::vector<T>& Y, std::vector<T>& Z, 
                std::vector<T>& U, std::vector<T>& V, std::vector<T>& W,
                T eps=1E-2, T bias=0.15, size_t steps=250, size_t iters=32) {

    assert(N > 2);

    std::ofstream ofile("data.bin", std::ios::binary);
    
    // constraints and temporary memory
    const T cnstr_two = X[N]-X[0];
    const T cnstr_dia = sqrt(2*cnstr_two*cnstr_two);    
    std::vector<T> tmp_X(X.size()), tmp_Y(Y.size()), tmp_Z(Z.size());

    // integration step for physics
    auto update_positions = [&](T& x, T& y ,T& z, T& u, T& v, T& w){
        
        w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : static_cast<T>(0);
        x += eps*u;
        y += eps*v;
        z += eps*w;
    };

    auto adjust_positions = [&](T& x, T& y ,T& z) {

        T rho = x*x+y*y+z*z;
        if (rho < 1) {
            rho = sqrt(rho);
            x /= rho;
            y /= rho;
            z /= rho;
        }
        z = std::max<T>(z, -1);
    };

    // Para: x, y and z
    // relaxation step for constraints
    auto relax_constraint = [&](size_t l, size_t m, T constraint){
        
        T delta_x = X[l]-X[m];
        T delta_y = Y[l]-Y[m];
        T delta_z = Z[l]-Z[m];

        T length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
        T displacement = (length-constraint)*bias;

        delta_x /=length;
        delta_y /=length;
        delta_z /=length;

        tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
        tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
        tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;
    };

    
    for (size_t step = 0; step < steps; step++) {
        
        // Para: Whole Mesh; Example pragma omp for twice
        // Stepwise dependency
        // let gravitiy do its job
        for (size_t i = 0; i < N; i++)
            for (size_t j = 0; j < N; j++)
                update_positions(X[i*N+j], Y[i*N+j], Z[i*N+j],
                                 U[i*N+j], V[i*N+j], W[i*N+j]);

        // fix the constraints
        for (size_t iter = 0; iter < iters; iter++) {

            std::copy(X.begin(), X.end(), tmp_X.begin());
            std::copy(Y.begin(), Y.end(), tmp_Y.begin());
            std::copy(Z.begin(), Z.end(), tmp_Z.begin());

            // Check constraints with mass point down (vertical)
            for (size_t i = 0; i < N-1; i++)
                // Can be parallelized (since each column is independent 
                // from each other.
                for (size_t j = 0; j < N; j++)
                    relax_constraint(i*N+j, (i+1)*N+j, cnstr_two);

            // Parallelize two i's (rows) at once (row is independent from the
            // direct neighbor. Two threads are possible.
            // Check constraints green vertical
            for (size_t i = 0; i < N-2; i++)
                // Can be parallelized
                for (size_t j = 0; j < N; j++)
                    relax_constraint(i*N+j, (i+2)*N+j, 2*cnstr_two);
            
            // Check constraints blue to the right
            // Can be parallelized since each row is independent.
            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N-1; j++)
                    relax_constraint(i*N+j, i*N+j+1, cnstr_two);

            // Can be parallelized since each row is independent.
            // Check constraints green to the right
            for (size_t i = 0; i < N; i++)
                // With 2 threads you can compute two columns at once.
                for (size_t j = 0; j < N-2; j++)
                    relax_constraint(i*N+j, i*N+j+2, 2*cnstr_two);

            // Can be parallelized row-wise or column-wise
            // Check constraints magenta right down
            for (size_t i = 0; i < N-1; i++)
                for (size_t j = 0; j < N-1; j++) 
                    relax_constraint(i*N+j, (i+1)*N+j+1, cnstr_dia);

            // Can be parallelized row-wise
            // Check constraints magenta right up
            for (size_t i = 1; i < N; i++)
                for (size_t j = 0; j < N-1; j++) 
                    relax_constraint(i*N+j, (i-1)*N+j+1, cnstr_dia);            

            // Both loops can be parallelized
            for (size_t i = 0; i < N; i++)
                for (size_t j = 0; j < N; j++)
                    adjust_positions(tmp_X[i*N+j], tmp_Y[i*N+j], tmp_Z[i*N+j]);

            std::copy(tmp_X.begin(), tmp_X.end(), X.begin());
            std::copy(tmp_Y.begin(), tmp_Y.end(), Y.begin());
            std::copy(tmp_Z.begin(), tmp_Z.end(), Z.begin());
        }

        if (dump) {
            // make sure you only do this once XD
            ofile.write((char*) X.data(), sizeof(T)*X.size());
            ofile.write((char*) Y.data(), sizeof(T)*Y.size());
            ofile.write((char*) Z.data(), sizeof(T)*Z.size());
        }
    }
}

int main(int argc, char const *argv[]) {

    /* for visualization, use ffmpeg to create videos
      
    import numpy as np
    import array as ar
    from mayavi import mlab

    with open("data.bin", "rb") as f:
        data = np.array(ar.array("d", f.read()))

    T = 250
    N = int(np.sqrt(len(data)/(3*T)))

    for cnt, (X, Y, Z) in enumerate(data.reshape((T, 3, N, N))):

        mlab.clf()
        mlab.points3d(0, 0, 0, scale_mode='none',scale_factor=1.95, resolution=50, color=(0,0,1) )
        mlab.mesh(X, Y, Z, color=(1, 0, 0), opacity=1.0)
        mlab.savefig(str(10000+cnt)+'.png', size=(1920, 1080))
    */

    std::vector<double> X, Y, Z, U, V, W;
    init_cloth<RES> (X, Y, Z, U, V, W);
    calc_cloth<RES> (X, Y, Z, U, V, W);

    std::cout << "this is mandatory to pass the test" << std::endl;

    return 0;
}