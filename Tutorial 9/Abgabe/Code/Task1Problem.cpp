auto update_positions = [&](T& x, T& y ,T& z, T& u, T& v, T& w){
        
    w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : static_cast<T>(0);
    x += eps*u;
    y += eps*v;
    z += eps*w;
};

auto relax_constraint = [&](size_t l, size_t m, T constraint){
        
    T delta_x = X[l]-X[m];
    T delta_y = Y[l]-Y[m];
    T delta_z = Z[l]-Z[m];

    T length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
    T displacement = (length-constraint)*bias;

    delta_x /=length;
    delta_y /=length;
    delta_z /=length;

    tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
    tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
    tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;
};

auto adjust_positions = [&](T& x, T& y ,T& z) {

    T rho = x*x+y*y+z*z;
    if (rho < 1) {
        rho = sqrt(rho);
        x /= rho;
        y /= rho;
        z /= rho;
    }
    z = std::max<T>(z, -1);
};