#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <immintrin.h>   // avx
#include <assert.h>
#include <chrono>
#include <cuda.h>

#define RES (20)


/*****************************************************************************
* COMPILE WITH nvcc -std=c++11  name.cu -arch=sm_20
*****************************************************************************/

void checkCUDAError(const char *msg);

void init_doubles(double* X, double* Y, double* Z) {
    int N = RES;
        for (size_t i = 0; i < N; i++) {
        double x = -2 + (4*i)/(N-1);
        for (size_t j = 0; j < N; j++) {
            double y = (-2) + (4*j)/(N-1);
            X[i*N+j] = x;
            Y[i*N+j] = y;
            Z[i*N+j] = 1;
        }
    }
}

__device__ void adjust_positions (double& x, double& y ,double& z) {
    
    double rho = x*x+y*y+z*z;
    if (rho < 1) {
        rho = sqrt(rho);
        x /= rho;
        y /= rho;
        z /= rho;
    }
    if(z < -1)
        z = -1;    
}

__device__ void update_positions(double& x, double& y ,double& z, double& u,
                                double& v, double& w, double eps){
    
    w = (x*x+y*y+z*z > 1 && z > -1) ? w-eps : (0);
    x += eps*u;
    y += eps*v;
    z += eps*w;
}

__device__ void relax_constraint (size_t l, size_t m, double constraint,
                            double* X, double* Y, double* Z,
                            double bias, double eps,
                            double* tmp_X, double* tmp_Y, double* tmp_Z) {
    
    double delta_x = X[l]-X[m];
    double delta_y = Y[l]-Y[m];
    double delta_z = Z[l]-Z[m];
    
    double length = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
    double displacement = (length-constraint)*bias;

    delta_x /=length;
    delta_y /=length;
    delta_z /=length;

    tmp_X[l] -= delta_x*displacement; tmp_X[m] += delta_x*displacement;
    tmp_Y[l] -= delta_y*displacement; tmp_Y[m] += delta_y*displacement;
    tmp_Z[l] -= delta_z*displacement; tmp_Z[m] += delta_z*displacement;   
}


__global__ void cuda_calc_falling(double* X, double* Y, double* Z, 
                double* U, double* V, double* W, int N,
                double eps=1E-2, double bias=0.15, 
                size_t steps=250, size_t iters=32) {
    
    // constraints and temporary memory
    int globalID = threadIdx.x+blockDim.x*blockIdx.x;
    
    double cnstr_two = X[N]-X[0];
    double cnstr_dia = sqrt(2*cnstr_two*cnstr_two);
    
    double tmp_X[RES*RES];
    double tmp_Y[RES*RES];
    double tmp_Z[RES*RES];
    
    for (size_t step = 0; step < steps; step++) {
        
       // let gravitiy do its job
        for (size_t i = 0; i < N; i++)
            update_positions(X[i*N+globalID], Y[i*N+globalID], Z[i*N+globalID],
                             U[i*N+globalID], V[i*N+globalID], W[i*N+globalID], eps);

        // fix the constraints
        for (size_t iter = 0; iter < iters; iter++) {
            
            for (size_t i = 0; i < N; i++) {
                tmp_X[globalID+i*N] = X[globalID+i*N];
                tmp_Y[globalID+i*N] = Y[globalID+i*N];
                tmp_Z[globalID+i*N] = Z[globalID+i*N];
            } 
            
            for (size_t i = 0; i < N-1; i++)
                relax_constraint(i*N+globalID, (i+1)*N+globalID, cnstr_two, 
                    X, Y, Z, bias, eps, tmp_X, tmp_Y, tmp_Z);

            for (size_t i = 0; i < N-2; i++)
                relax_constraint(i*N+globalID, (i+2)*N+globalID, 2*cnstr_two, 
                    X, Y, Z, bias, eps, tmp_X, tmp_Y, tmp_Z);
            
            for (size_t j = 0; j < N-1; j++)
                relax_constraint(globalID*N+j, globalID*N+j+1, cnstr_two, 
                    X, Y, Z, bias, eps, tmp_X, tmp_Y, tmp_Z);

            for (size_t j = 0; j < N-2; j++)
                relax_constraint(globalID*N+j, globalID*N+j+2, 2*cnstr_two, 
                    X, Y, Z, bias, eps, tmp_X, tmp_Y, tmp_Z);

            for (size_t j = 0; j < N-1; j++) {
                if(globalID < N-1)
                    relax_constraint(globalID*N+j, (globalID+1)*N+j+1, cnstr_dia, 
                    X, Y, Z, bias, eps, tmp_X, tmp_Y, tmp_Z);
            }

            for (size_t j = 0; j < N-1; j++) {
                if(globalID > 0)
                    relax_constraint((globalID+1)*N+j, globalID*N+j+1, cnstr_dia, 
                    X, Y, Z, bias, eps, tmp_X, tmp_Y, tmp_Z);  
            }

            for (size_t j = 0; j < N; j++)
                adjust_positions(tmp_X[globalID*N+j], tmp_Y[globalID*N+j], 
                                 tmp_Z[globalID*N+j]);
        
            for (size_t i = 0; i < N; i++) {
                X[globalID+i*N] = tmp_X[globalID+i*N];
                Y[globalID+i*N] = tmp_Y[globalID+i*N];
                Z[globalID+i*N] = tmp_Z[globalID+i*N];
            } 
        } 
    } 
}

int main(int argc, char const *argv[]) {
    
    double * X = new double[RES*RES];
    double * Y = new double[RES*RES];
    double * Z = new double[RES*RES];
    double * U = new double[RES*RES];
    double * V = new double[RES*RES];
    double * W = new double[RES*RES];
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed_seconds;
    
    init_doubles(X, Y, Z);
    
    double * device_X = new double[RES*RES];
    double * device_Y = new double[RES*RES];
    double * device_Z = new double[RES*RES];
    double * device_U = new double[RES*RES];
    double * device_V = new double[RES*RES];
    double * device_W = new double[RES*RES];
    cudaMalloc(&device_X, sizeof(double)*RES*RES);
    cudaMalloc(&device_Y, sizeof(double)*RES*RES);
    cudaMalloc(&device_Z, sizeof(double)*RES*RES);
    cudaMalloc(&device_U, sizeof(double)*RES*RES);
    cudaMalloc(&device_V, sizeof(double)*RES*RES);
    cudaMalloc(&device_W, sizeof(double)*RES*RES);
    double memory_consumption = 8*RES*RES*9;
    std::cout << "Memory consumption: " << memory_consumption << std::endl;
    std::cout << "kByte " << memory_consumption/1024 << std::endl;
    int nBlock = 1;
    // All threads can compute either a row or a column at once. Not both.
    int nThread = RES;
    // Just arbitrary code. Not needed in our case
    if(RES > 1024) {
        nThread = 1024;
        nBlock = ((RES+1023)/1024);
    }
    
    cudaMemcpy(device_X, X, sizeof(double)*RES*RES, cudaMemcpyHostToDevice);
    checkCUDAError("cudaMemcpy: host to device\n");
    cudaMemcpy(device_Y, Y, sizeof(double)*RES*RES, cudaMemcpyHostToDevice);
    checkCUDAError("cudaMemcpy: host to device\n");
    cudaMemcpy(device_Z, Z, sizeof(double)*RES*RES, cudaMemcpyHostToDevice);
    checkCUDAError("cudaMemcpy: host to device\n");
    
    start = std::chrono::system_clock::now();
    
    cuda_calc_falling<<<nBlock, nThread>>>(device_X, device_Y, device_Z, device_U, device_V, device_W, RES);
    cudaDeviceSynchronize();
    checkCUDAError("kernel launching\n");
    
    cudaMemcpy(X, device_X, sizeof(double)*RES*RES, cudaMemcpyDeviceToHost);
    checkCUDAError("cudaMemcpy: device to host\n"); 
    cudaMemcpy(Y, device_Y, sizeof(double)*RES*RES, cudaMemcpyDeviceToHost);
    checkCUDAError("cudaMemcpy: device to host\n"); 
    cudaMemcpy(Z, device_Z, sizeof(double)*RES*RES, cudaMemcpyDeviceToHost);
    checkCUDAError("cudaMemcpy: device to host\n"); 
    
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    
    std::cout << "calctime: " << elapsed_seconds.count() << " s"  << std::endl;
    /*
    std::ofstream ofile("dataCuda.bin", std::ios::binary);
    ofile.write((char*) X, sizeof(double)*RES*RES);
    ofile.write((char*) Y, sizeof(double)*RES*RES);
    ofile.write((char*) Z, sizeof(double)*RES*RES);
    */
    std::cout << "programm terminated" << std::endl;
    
    return 0;
}

//function to check cuda error, cited from 
//http://www.drdobbs.com/parallel/cuda-supercomputing-for-the-masses-part/207603131?pgno=2
void checkCUDAError(const char *msg)
{
    cudaError_t err = cudaGetLastError();
    if( cudaSuccess != err) 
    {
        fprintf(stderr, "Cuda error: %s: %s.\n", msg, 
                                  cudaGetErrorString( err) );
        exit(EXIT_FAILURE);
    }                         
}