\contentsline {section}{1 Mutexes versus Atomics (6P)}{3}
\contentsline {subsection}{(i) Explain why the function \texttt {false\_inc} almost always produces the wrong result.}{3}
\contentsline {subsection}{(ii) Write a function \texttt {mutex\_inc} using a C++11 mutex that calculates the correct result. To achieve that, extend the provided SAUCE template.}{3}
\contentsline {subsection}{(iii) The C++11 specification provides so-called atomics for integral data types. Alter the code using an atomic sum-variable such that \texttt {false\_inc} produces the correct result.}{4}
\contentsline {subsection}{(iv) Discuss the execution speed of all three variants.}{4}
\contentsline {section}{2 Simulating Join with Condition Variables (6P)}{9}
\contentsline {section}{3 Playing Ping Pong with Condition Variables (8P)}{10}
