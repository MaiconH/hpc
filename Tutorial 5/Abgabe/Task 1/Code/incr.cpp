#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <atomic>
#include <mutex>

#define NUM_REPEATS (10000000L)
#define NUM_PROCS (6)

std::mutex mutex;

template <class T>
void false_inc(T * sum_ptr) {

    for (size_t i = 0; i < NUM_REPEATS; i++)
        (*sum_ptr)++;
}

template <class T>
void mutex_inc(T * sum_ptr) {
    // you know what to do
    for (size_t i = 0; i < NUM_REPEATS; i++){
        mutex.lock();
        (*sum_ptr)++;
        mutex.unlock();
    }
}

// leave this as it is
template <class T, class F>
void benchmark(T * sum_ptr, F f, std::string method_name) {

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    
    
    std::thread threads[NUM_PROCS];
    
    for (size_t i = 0; i < NUM_PROCS; i++)
        threads[i] = std::thread(f, sum_ptr);
        
    for (size_t i = 0; i < NUM_PROCS; i++)
        threads[i].join();

     end = std::chrono::system_clock::now();
     std::chrono::duration<double> elapsed_seconds = end-start;
     std::cout << "# elapsed time (" << method_name << "): "
               << elapsed_seconds.count()  << "s" << std::endl;

    std::cout << "# sums: " << *sum_ptr << " vs. expected "
              << NUM_PROCS*NUM_REPEATS << std::endl;
}

int main ( ) {

    long sum = 0;
    
    benchmark(&sum, false_inc<decltype(sum)>, "false_inc");
    
    long msum = 0;    
    benchmark(&msum, mutex_inc<decltype(msum)>, "mutex_inc");

    // define an atomic sum variable "asum" here
    // benchmark(&asum, false_inc<decltype(asum)>, "atomic_inc");

    std::atomic_long asum(0);
    benchmark(&asum, false_inc<decltype(asum)>, "atomic_inc");
    
    std::cout << "programme terminated" << std::endl;
}