#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <atomic>
#include <mutex>
#include <fstream>

#define NUM_REPEATS (10000000L)
#define NUM_ROUNDS (100)
#define NUM_PROCS (6)

std::mutex mutex;

template <class T>
void false_inc(volatile T * sum_ptr) {

    for (size_t i = 0; i < NUM_REPEATS; i++)
        (*sum_ptr)++;
}

template <class T>
void mutex_inc(T * sum_ptr) {
    // you know what to do
    for (size_t i = 0; i < NUM_REPEATS; i++){
        mutex.lock();
        (*sum_ptr)++;
        mutex.unlock();
    }
}

// leave this as it is
template <class T, class F>
void benchmark(T * sum_ptr, F f, std::string method_name) {

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    
    
    std::thread threads[NUM_PROCS];
    
    for (size_t i = 0; i < NUM_PROCS; i++)
        threads[i] = std::thread(f, sum_ptr);
        
    for (size_t i = 0; i < NUM_PROCS; i++)
        threads[i].join();

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    //std::cout << "# elapsed time (" << method_name << "): "
    //           << elapsed_seconds.count()  << "s" << std::endl;

    //std::cout << "# sums: " << *sum_ptr << " vs. expected "
    //          << NUM_PROCS*NUM_REPEATS << std::endl;
    std::ofstream task1Bench("task1Bench", std::ios_base::app);
    task1Bench << elapsed_seconds.count() << ";" << *sum_ptr << ";" << 
        NUM_PROCS*NUM_REPEATS << ";";
    if(method_name == "atomic_inc")
        task1Bench << "\n";
    task1Bench.close();
    
}

int main ( ) {
    std::ofstream task1Bench("task1Bench", std::ios_base::app);
    task1Bench << "false_inc;sum;expectedSum;mutex_inc;sum;expectedSum;" << 
        "atomic_inc;sum;expectedSum\n";
    task1Bench.close();
    
    for(int i=0; i<NUM_ROUNDS; i++) {
                
        long sum = 0;
    
        benchmark(&sum, false_inc<decltype(sum)>, "false_inc");
    
        long msum = 0;    
        benchmark(&msum, mutex_inc<decltype(msum)>, "mutex_inc");

        // define an atomic sum variable "asum" here
        // benchmark(&asum, false_inc<decltype(asum)>, "atomic_inc");

        std::atomic_long asum(0);
        benchmark(&asum, false_inc<decltype(asum)>, "atomic_inc");
        
    }

    
    std::cout << "programme terminated" << std::endl;
}