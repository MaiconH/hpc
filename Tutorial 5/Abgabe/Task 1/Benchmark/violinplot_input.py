"""
Usage: python violinplot_input.py input output datalength 
"""

import random
import sys
import numpy as np
import matplotlib.pyplot as plt

# fake data
fs = 10 # fontsize

pos = [1, 2, 3]
data = [[] for i in range(3)]
# read input file

with open(sys.argv[1], 'r') as f:
    values = f.readlines()
    for line in values:
        words = line.split(";")
        #print(words)
        #print(words[0])
        #print(words[1])
        data[0].append(float(words[0])) 
        data[1].append(float(words[3]))
        data[2].append(float(words[6]))

plt.violinplot(data, pos, points=sys.argv[3], vert=False, widths=1.1,
                      showmeans=True, showextrema=True, showmedians=True,
                      bw_method=0.5)
plt.suptitle(sys.argv[4], fontsize=fs)
plt.xlabel('computation time in seconds', fontsize=fs)

plt.savefig(sys.argv[2], dpi=200)
