#include <mutex>
#include <pthread.h>

pthread_mutex_t pang_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t ping_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t pong_cond = PTHREAD_COND_INITIALIZER;
bool first_printed = false;
int pairs = 50;

// admire the void
void* pong(void *arg) {

    for(int i = 0; i < pairs; i++) {
      //  pthread_mutex_lock(&pang_mutex);
        if(!first_printed) {
   //         pthread_mutex_unlock(&pang_mutex);
     //       pthread_cond_wait(&ping_cond, &pang_mutex);
   //         pthread_mutex_lock(&pang_mutex); 
        } 
        printf("pong\n");
    //    pthread_cond_signal(&pong_cond);
        first_printed = false;
    //    pthread_mutex_unlock(&pang_mutex);
    }
}

void* ping(void *arg) {
    
    for (int j = 0; j < pairs; j++) {
       // pthread_mutex_lock(&pang_mutex);
        if(first_printed) {
      //      pthread_mutex_unlock(&pang_mutex);
       //     pthread_cond_wait(&pong_cond, &pang_mutex);
      //      pthread_mutex_lock(&pang_mutex); 
        } 
        printf("ping\n");
    //    pthread_cond_signal(&ping_cond);
        first_printed = true;
     //   pthread_mutex_unlock(&pang_mutex);
    }   
}

int main () {
    
    pthread_t ping_thread;
    pthread_t pong_thread;

    pthread_create(&ping_thread, NULL, ping, NULL);
    pthread_create(&pong_thread, NULL, pong, NULL);
    pthread_join(ping_thread, NULL);
    pthread_join(pong_thread, NULL);
}