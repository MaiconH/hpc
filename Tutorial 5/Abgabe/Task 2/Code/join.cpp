#include <iostream>
#include <pthread.h>

// First part
bool done = false;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;

// Third part: Lock m and wait for the signal with pthread_cond_wait as long
// as done == false. Then unlock m.
void join() {
    // fill your magic in here
    pthread_mutex_lock(&m);
    while(!done) {
        pthread_cond_wait(&c, &m);
    }
    pthread_mutex_unlock(&m);
}

void * child(void *arg) {
    std::cout << "child" << std::endl;
  
    // here is also something missing
    pthread_mutex_lock(&m);
    done = true;
    pthread_cond_broadcast(&c);
    pthread_mutex_unlock(&m);
}

int main(int argc, char *argv[]) {
    std::cout << "parent" << std::endl;
    
    pthread_t p;
    pthread_create(&p, NULL, child, NULL);
    // Second part: Lock the mutex m and set done = true
    // Afterwards signal the change in state to c.
    // Unlock m
    
    //pthread_join(p, NULL);
    join();
    
    std::cout << "parent" << std::endl;
}
