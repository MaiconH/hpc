#include <immintrin.h>   // avx
#include <iostream>      // std::cout
#include <chrono>        // time measurement
#include <string>

#define NUM_REPEATS (1000)

/*****************************************************************************
* COMPILE WITH g++ -O3 -march=native -pthread -mavx -std=c++11 matrix_add.cpp
*****************************************************************************/

// fills matrices with indices in row-major-order
void init_matrices(float * A, size_t N) {
    
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            A[i*N+j] = i*N+j;
}

// checks if C = A + B
void check_matrices(float * A, float * B, float * C, size_t N) {
    
     for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if( A[i*N+j] + B[i*N+j] != C[i*N+j]) {
                std::cout << "error at " << i << ", " << j << std::endl;
                return;
            }
}

// repeats addition NUM_REPEATS times and reports wall clock time
template <class F>
void bench_add(float * A, float * B, float * C, size_t N, 
               F function, std::string method_name) {
    
    init_matrices(A, N);
    init_matrices(B, N);
    init_matrices(C, N);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
     
    for (int i = 0; i < NUM_REPEATS; i++)
        function(A, B, C, N);
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (" << method_name << "): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    check_matrices(A, B, C, N);
}

// row-major-order addition
void row_wise_add(float * A, float * B, float * C, size_t N) {
    
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            C[i*N+j] = A[i*N+j] + B[i*N+j];

}

// col-major-order addition
void col_wise_add(float * A, float * B, float * C, size_t N) {
    
    for (int j = 0; j < N; j++)
        for (int i = 0; i < N; i++)
            C[i*N+j] = A[i*N+j] + B[i*N+j];
}

// avx accelerated row-major-order addition
void avx_vec8_add(float * A, float * B, float * C, size_t N) {

    // your Code here
    for (int i=0; i<N; i++)
    {
        for (int j=0; j<N; j+=8)
        {
            __m128 tmpA = _mm128_load_ps(&A[i*N+j]);
            __m128 tmpB = _mm128_load_ps(&B[i*N+j]);
            _mm128_store_ps(&C[i*N+j], tmpA+tmpB);
        }
    }
}

int main() {

    size_t N = 1024; // length of matrix
    
    // memory must be aligned to allow for the use of avx
    float * A = (float *)_mm_malloc(N*N*sizeof(float), 32);
    float * B = (float *)_mm_malloc(N*N*sizeof(float), 32);
    float * C = (float *)_mm_malloc(N*N*sizeof(float), 32);

    bench_add(A, B, C, N, col_wise_add, "col_wise_add");
    bench_add(A, B, C, N, row_wise_add, "row_wise_add");
    bench_add(A, B, C, N, avx_vec8_add, "avx_vec8_add");

    // avx equivalent of delete []
    _mm_free(A);
    _mm_free(B);
    _mm_free(C);

    std::cout << "programme terminated" << std::endl;
}

