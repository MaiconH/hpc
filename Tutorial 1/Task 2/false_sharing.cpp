#include <iostream>  // std::cout
#include <thread>    // C++11 pthreads
#include <chrono>    // time measurements

#define NUM_REPEATS (1000)

/*****************************************************************************
* COMPILE WITH g++ -O3 -march=native -pthread -mavx -std=c++11 false_sharing.cpp
*****************************************************************************/

// fills matrices with indices in row-major-order
void init_matrices(float * A, size_t N) {
    
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            A[i*N+j] = i*N+j;
}

// checks if C = A + B
void check_matrices(float * A, float * B, float * C, size_t N) {
    
     for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if(A[i*N+j] + B[i*N+j] != C[i*N+j]) {
                std::cout << "error at " << i << ", " << j << std::endl;
                return ;
            }
}

// repeats addition NUM_REPEATS times and reports wall clock time
template <class F>
void bench_add(float * A, float * B, float * C, size_t N, size_t P,
               F function, std::string method_name) {
    
    init_matrices(A, N);
    init_matrices(B, N);
    init_matrices(C, N);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
     
    for (int i = 0; i < NUM_REPEATS; i++)
        function(A, B, C, N, P);
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (" << method_name << "): "
              << elapsed_seconds.count()  << "s" << std::endl;
              
    check_matrices(A, B, C, N);
}

// adds entries (threadID, threadID+P, ...)
void add_interleaved(float * A, float * B, float * C, size_t N, 
                     size_t P, size_t threadID) {
    
    for (int i = 0; i < N; i++)
        for (int j = threadID; j < N; j += P)
            C[i*N+j] = A[i*N+j] + B[i*N+j];

}

// spawns P threads and calls add_interleaved for each threadID
void false_sharing_add(float * A, float * B, float * C, size_t N, size_t P) {

    std::thread threads[P];
    
    for (int i = 0; i < P; i++)
        threads[i] = std::thread(add_interleaved, A, B, C, N, P, i);
    
    for (int i = 0; i < P; i++)
        threads[i].join();
}

void add_coalesced(float * A, float * B, float * C, size_t N, 
                     size_t P, size_t threadID) 
{
    int missedValues = N % P;
    int batch = N/P;
    for (int i = 0; i < N; i++)
    {
        for (int j = threadID*batch; j < (threadID*batch)+batch; j++)
            C[i*N+j] = A[i*N+j] + B[i*N+j];
        // Needed if N/P does not cover all entries. Each processor calculates one
        // value while at least one is idle
        if(threadID < missedValues)
            C[N*i+N-threadID-1] = A[N*i+N-threadID-1] + B[N*i+N-threadID-1];
    }
}

void coalesced_mem_add(float * A, float * B, float * C, size_t N, size_t P) {

    std::thread threads[P];
    for (int i = 0; i < P; i++)
        threads[i] = std::thread(add_coalesced, A, B, C, N, P, i);
    
    for (int i = 0; i < P; i++)
        threads[i].join();
}
int main() {

    size_t N = 1024; // length of matrix
    size_t P = 6;    // maximum number of processors
    
    float * A = new float[N*N];
    float * B = new float[N*N];
    float * C = new float[N*N];

    // test speed for false sharing and coalesced memory access
    for (int p = 1; p <= P; p++) {
        std::cout << "# = parallel with " << p << " processors =" << std::endl;
        
        bench_add(A, B, C, N, p, false_sharing_add, "false_sharing_add");
        bench_add(A, B, C, N, p, coalesced_mem_add,  "coalesced_mem_add");
    }
    
    delete [] A;
    delete [] B;
    delete [] C;
    
    std::cout << "programme terminated" << std::endl;
}
