#include <iostream>  // std::cout
#include <thread>    // C++11 pthreads
#include <chrono>    // time measurements
#include <cuda.h>    // Cuda for GPU usage

#define NUM_REPEATS (1000)

/*****************************************************************************
* COMPILE WITH nvcc -std=c++11  false_sharing.cu -arch=sm_20
*****************************************************************************/
void checkCUDAError(const char *msg);

__global__ void cuda_mem_add(float* A, float* B, float* C, size_t N)
{
    int globalId = threadIdx.x+blockDim.x*blockIdx.x;
    if(globalId < N*N)
        C[globalId] = A[globalId] + B[globalId];
}

// fills matrices with indices in row-major-order
void init_matrices(float * A, size_t N) {
    
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            A[i*N+j] = i*N+j;
}

// checks if C = A + B
void check_matrices(float * A, float * B, float * C, size_t N) {
    
     for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            if(A[i*N+j] + B[i*N+j] != C[i*N+j]) {
                std::cout << "error at " << i << ", " << j << std::endl;
                return ;
            }
}

// repeats addition NUM_REPEATS times and reports wall clock time
void bench_add_cuda(float * A, float * B, float * C, size_t N, 
                    std::string method_name) 
{
    float* deviceA = new float[N*N];
    float* deviceB = new float[N*N];
    float* deviceC = new float[N*N];
    cudaMalloc(&deviceA, sizeof(float)*N*N);
    cudaMalloc(&deviceB, sizeof(float)*N*N);
    cudaMalloc(&deviceC, sizeof(float)*N*N);
    init_matrices(A, N);
    init_matrices(B, N);
    init_matrices(C, N);
    int nBlock = 1;
    int nThread = N*N;
    if(N*N>1024)
    {
        nThread = 1024;
        nBlock = ((N*N+1023)/1024);
    }
    cudaMemcpy(deviceA, A, sizeof(float)*N*N, cudaMemcpyHostToDevice);
    checkCUDAError("cudaMemcpy: host to device\n");
    cudaMemcpy(deviceB, B, sizeof(float)*N*N, cudaMemcpyHostToDevice);
    checkCUDAError("cudaMemcpy: host to device\n");
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    for (int i = 0; i < NUM_REPEATS; i++)
    {

        cuda_mem_add<<<nBlock, nThread>>>(deviceA, deviceB, deviceC, N);
        cudaDeviceSynchronize();
        checkCUDAError("kernel launching\n");

    }
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (" << method_name << "): "
              << elapsed_seconds.count()  << "s" << std::endl;
    cudaMemcpy(C, deviceC, sizeof(float)*N*N, cudaMemcpyDeviceToHost);
    checkCUDAError("cudaMemcpy: device to host\n"); 
    check_matrices(A, B, C, N);
}



int main() {

    size_t N = 1024; // length of matrix
    
    float * A = new float[N*N];
    float * B = new float[N*N];
    float * C = new float[N*N];

    bench_add_cuda(A, B, C, N, "cuda_mem_add");

    delete [] A;
    delete [] B;
    delete [] C;
    
    std::cout << "programme terminated" << std::endl;
}

//
//function to check cuda error, cited from 
//http://www.drdobbs.com/parallel/cuda-supercomputing-for-the-masses-part/207603131?pgno=2
void checkCUDAError(const char *msg)
{
    cudaError_t err = cudaGetLastError();
    if( cudaSuccess != err) 
    {
        fprintf(stderr, "Cuda error: %s: %s.\n", msg, 
                                  cudaGetErrorString( err) );
        exit(EXIT_FAILURE);
    }                         
}
