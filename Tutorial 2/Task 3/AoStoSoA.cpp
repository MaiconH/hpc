#include <immintrin.h>   // avx
#include <iostream>      // std::cout
#include <cstdlib>       // std::rand
#include <chrono>        // time measurement
#include <thread>        // std::thread
#include <cmath>         // exp
#include <string>
#include <iostream>
#include <fstream>


#define NUM_REPEATS (10)
#define NUM_THREADS (4)
#define SEED (42)
#define NUM_ROUNDS (50)
/*****************************************************************************
* COMPILE WITH g++ -mavx -std=c++11 AoStoSoA.cpp
*****************************************************************************/
double countedTime[NUM_ROUNDS*2];
int times = 0;
void init_A(float * A, size_t N) {
    
    std::srand(SEED);
    
    for (int i = 0; i < 3*N; i++)
        A[i] = 2.0f*std::rand()/RAND_MAX-1.0f;
}

void init_B(float * B, size_t N) {
    
    for (int i = 0; i < N; i++)
        B[i] = 0.0f;
}

void check(float * A, float * B, size_t N) {

    for (int i = 0; i < N; i++) {
        float x = A[3*i];
        float y = A[3*i+1];
        float z = A[3*i+2];
        float r = 1.0f/sqrtf(x*x+y*y+z*z);
        float s = r - B[i];
        
        if (s*s > 1E-3*r*r) {
            std::cout << "error at index " << i << std::endl;
            return;
        }
    }
}

void naive_calc(float * A, float * B, size_t N) {
    
    for (int i = 0; i < N; i++) {
        float x = A[3*i];
        float y = A[3*i+1];
        float z = A[3*i+2];
        B[i] = 1.0f/sqrtf(x*x+y*y+z*z);
    }
}

template <class F>
void bench(float * A, float * B, size_t N, 
               F function, std::string method_name) {
    
    init_A(A, N);
    init_B(B, N);
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
     
    for (int i = 0; i < NUM_REPEATS; i++)
        function(A, B, N);
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    /*std::cout << "# elapsed time (" << method_name << "): "
              << elapsed_seconds.count()  << "s" << std::endl; */
    countedTime[times] = elapsed_seconds.count();
    times++;
    //check(A, B, N);
}

void soavx_calc(float * A, float * B, size_t N) 
{
    for (int i = 0; i < N/8; i++) 
    {
        // Shuffle AoS to SoA
        __m128 *m = (__m128*) (A+i*8*3);
        __m256 a03;
        __m256 a14;
        __m256 a25;
        a03 = _mm256_castps128_ps256(m[0]);
        a14 = _mm256_castps128_ps256(m[1]);
        a25 = _mm256_castps128_ps256(m[2]);

        a03 = _mm256_insertf128_ps(a03, m[3],1);
        a14 = _mm256_insertf128_ps(a14, m[4],1);
        a25 = _mm256_insertf128_ps(a25, m[5],1);
        __m256 xy = _mm256_shuffle_ps(a14, a25, _MM_SHUFFLE( 2,1,3,2)); 
        __m256 yz = _mm256_shuffle_ps(a03, a14, _MM_SHUFFLE( 1,0,2,1)); 
        __m256 x = _mm256_shuffle_ps(a03, xy , _MM_SHUFFLE( 2,0,3,0));
        __m256 y = _mm256_shuffle_ps(yz , xy , _MM_SHUFFLE( 3,1,2,0));
        __m256 z = _mm256_shuffle_ps(yz , a25, _MM_SHUFFLE( 3,0,3,1));  
        // Normalization
        __m256 xxyy = _mm256_add_ps(_mm256_mul_ps(x,x),_mm256_mul_ps(y,y));
        __m256 xxyyzz = _mm256_add_ps(_mm256_mul_ps(z,z),xxyy);
        __m256 inverseRoot = _mm256_rsqrt_ps(xxyyzz);
        _mm256_store_ps(&B[i*8], inverseRoot);
       
    }
}

int main (int argc, char * argv[]) {

    size_t N = 1024*2*3*4*5*6*100; // number of coordinates
    
    // memory must be aligned to allow for the use of avx
    float * A = (float *)_mm_malloc(3*N*sizeof(float), 32);
    float * B = (float *)_mm_malloc(  N*sizeof(float), 32);
    for(int i=0; i<NUM_ROUNDS; i++){
        bench(A, B, N, naive_calc, "naive_calc");

    }
    for(int i=0; i<NUM_ROUNDS; i++)
        bench(A, B, N, soavx_calc, "soavx_calc");
    

    // avx equivalent of delete []
    _mm_free(A);
    _mm_free(B);
    
    std::ofstream timeTable;
    timeTable.open ("timeTable", std::ios_base::app);
    for(int i=0; i<NUM_ROUNDS; i++)
    {
        timeTable << countedTime[i] << ";" << countedTime[i+NUM_ROUNDS] << "\n";
    }
    timeTable.close();
    std::cout << "programme terminated" << std::endl;
}

