#include <algorithm>
#include <iostream>
#include <random>
#include <atomic>
#include <thread>
#include <assert.h>

#define N (32)                     // number of items limited to 32
#define C (1500)                   // maximal allowed capacity

#define LO32(x) (x & 4294967295)   // project to lower 32 bits of a long
#define HI32(x) (x >> 32)          // project to higher 32 bits of a long
#define PACK(x, y) (x+(static_cast<unsigned long>(y) << 32)) // pack LO and HI

// needed to sort the quotients of V and W
typedef std::pair<unsigned int, float> Ratio;

std::vector<unsigned int> W;       // vector of weights
std::vector<unsigned int> V;       // vector of values
std::atomic<unsigned long> S(0);   // initial (global) state and lower bound

////////////////////////////////////////////////////////////////////////////////

// explain what this magic lock-free update function does
void update(const unsigned long& value) noexcept {

    unsigned long tmp = S;
    while(HI32(S) < HI32(value) && 
            S.compare_exchange_weak(tmp, value)){}
// I thinks this is wrong
//    if (HI32(S) < HI32(value))
//        S = value; // this could be dangerous if we use threads! Why is that so? See 1 (race cond. between compare and write)

}

// initializes Knapsack problem
void init_problem() {

    // C++11 random number generator
    std::mt19937 engine(0);
    std::uniform_int_distribution<int> density(80, 100);
 
    // vector of ratios (needed for keysort)
    std::vector<Ratio> R;
   
    // generate a problem
    for (unsigned int i = 0; i < N; i++) {
        
        const unsigned int w = density(engine),
                           v = density(engine);

        W.push_back(w);
        V.push_back(v);
        R.push_back(Ratio(i, static_cast<float>(v)/static_cast<float>(w)));
    }
    
    // keysort R by ratio for upper bound estimate (we really need zip in C++)
    std::sort(R.begin(), R.end(), [] (const Ratio& a, const Ratio& b)
                                     { return a.second> b.second; });

    // rearrange V und W according to the indices of sorted R
    std::vector<unsigned int> newV(N);
    std::vector<unsigned int> newW(N);
    std::transform(R.begin(), R.end(), newV.begin(),
                   [] (const Ratio& x) { return V[x.first]; });
    std::transform(R.begin(), R.end(), newW.begin(),
                   [] (const Ratio& x) { return W[x.first]; });
    V = newV;
    W = newW;
}

unsigned int upper_bound(unsigned int height,   // height of the binary tree
                         unsigned int value,    // accumulated value until h
                         unsigned int weight) { // accumulated weight until h

    for (int index = height+1; index < N && weight <= C; index++) {
        value  += V[index];
        weight += W[index];
    }

    return value;
}

void traverse(const unsigned int height,        // height of the binary tree
              const unsigned int old_value,     // accumulated value until h-1
              const unsigned int old_weight,    // accumulated weight until h-1
              const unsigned int state) {       // encodes local state
//////////////////////////////////////////////////////////////////////////////// 
// Assignment ii
    // I guess it's somehow recursive
    // Code by Tassilo
    int value = old_value;
    int weight = old_weight;

    value += V[height]*((state>>height)&1);
    weight += W[height]*((state>>height)&1);
    //exceed capacity == terminate branch
    if(weight > C)return;
    //update global_upper_bound if necessary
    if(HI32(S) < value){
        unsigned long local_lower_bound = value;
        local_lower_bound <<= 32;
        local_lower_bound += state;
        update(local_lower_bound);
    }
    //check upper bound greedily
    if(upper_bound(height,value,weight) <= HI32(S)) return;
    //recursive call
    traverse(height+1, value, weight, state);
    traverse(height+1, value, weight, state | 1 << height+1);
//////////////////////////////////////////////////////////////////////////////// 
}

void decompose_state(const unsigned long& state) {

    unsigned int value = 0;
    unsigned int weight = 0;
    unsigned int status = LO32(state);

    std::cout << std::endl << "best packing: " << std::endl << "S = ";
    for (int i = 0; i < N; i++) {
        bool bit = (status & 1);
        weight += bit*W[i];
        value  += bit*V[i];
        std::cout << bit << "\t";
        status >>= 1;
    }
    std::cout << std::endl;
    std::cout << "V = " << HI32(state) << " (Test: " << value
              << ") , W = " << weight << std::endl;
}

void sequential_solve() {

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    
    S = 0;

    traverse(0, 0, 0, 0);
    traverse(0, 0, 0, 1);
    
    end = std::chrono::system_clock::now();

    decompose_state(S);
    
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
               << elapsed_seconds.count()  << "s" << std::endl;
    
    
}

void parallel_solve() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    S = 0;

    std::thread t1 = std::thread(traverse, 0, 0, 0, 1);
    std::thread t2 = std::thread(traverse, 0, 0, 0, 0);
    t1.join();
    t2.join();

    end = std::chrono::system_clock::now();

    decompose_state(S);

    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel h=1): "
               << elapsed_seconds.count()  << "s" << std::endl;
}


void parallel_solve_h(unsigned int h) {
    
    assert(0 < h && h < 32); 
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    S = 0;
    
    std::vector<unsigned int> weights (1 << h, 0);
    std::vector<unsigned int> values  (1 << h, 0);
    for (unsigned int i = 0; i < (1 << h); i++) {
        // mystic commands
////////////////////////////////////////////////////////////////////////////////
    // Assignment iv
////////////////////////////////////////////////////////////////////////////////
    }
    
    std::vector<std::thread> threads;
    for (unsigned int i = 0; i < (1 << h); i++)
        threads.push_back(std::thread(traverse, h-1, values[i], weights[i], i)); 

    for (auto& thread : threads)
        thread.join();
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

    decompose_state(S);
    
    std::cout << "# elapsed time (parallel h=" <<  h << "): "
               << elapsed_seconds.count()  << "s" << std::endl;
}
////////////////////////////////////////////////////////////////////////////////

int main() {

    // constrains for the integer packing
    assert(sizeof(long) == 8);
    assert(N <= 32);

    init_problem();   // initialize V, W, 

    std::cout << "V = ";
    for (auto& v: V)
        std::cout << v << "\t";
    std::cout << std::endl;

    std::cout << "W = ";
    for (auto& w: W)
        std::cout << w << "\t";
    std::cout << std::endl;

    sequential_solve();
    parallel_solve();
    
    for (int h = 2; h < 15; h++)
        parallel_solve_h(h);

}