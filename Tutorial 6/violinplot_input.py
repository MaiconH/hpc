"""
Usage: python violinplot_input.py input output datalength 
"""

import random
import sys
import numpy as np
import matplotlib.pyplot as plt

# fake data
fs = 10 # fontsize

pos = [2, 4, 6, 7, 8, 9, 10, 12, 14]
data = [[] for i in range(9)]
# read input file

with open(sys.argv[1], 'r') as f:
    values = f.readlines()
    for line in values:
        words = line.split(";")
        #print(words)
        #print(words[0])
        #print(words[12])
        data[0].append(float(words[2]))
        data[1].append(float(words[4]))
        data[2].append(float(words[6]))
        data[3].append(float(words[7]))
        data[4].append(float(words[8]))
        data[5].append(float(words[9]))
        data[6].append(float(words[10]))
        data[7].append(float(words[12]))
        data[8].append(float(words[14])) 
       # data[0].append(float(words[1]))  
        
      # data[8].append(float(words[14]))

plt.violinplot(data, pos, points=sys.argv[3], vert=False, widths=1.1,
                      showmeans=True, showextrema=True, showmedians=True,
                      bw_method=0.5)
plt.suptitle('Parallel computation with h in the y-axis', fontsize=fs)
plt.xlabel('computation time in seconds', fontsize=fs)

plt.savefig(sys.argv[2], dpi=200)
