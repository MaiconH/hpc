#include <algorithm>
#include <iostream>
#include <random>
#include <atomic>
#include <thread>
#include <assert.h>

#define N (32)                     // number of items limited to 32
#define C (1500)                   // maximal allowed capacity

#define LO32(x) (x & 4294967295)   // project to lower 32 bits of a long
#define HI32(x) (x >> 32)          // project to higher 32 bits of a long
#define PACK(x, y) (x+(static_cast<unsigned long>(y) << 32)) // pack LO and HI

// needed to sort the quotients of V and W
typedef std::pair<unsigned int, float> Ratio;

std::vector<unsigned int> W;       // vector of weights
std::vector<unsigned int> V;       // vector of values
std::atomic<unsigned long> S(0);   // initial (global) state and lower bound

////////////////////////////////////////////////////////////////////////////////

// explain what this magic lock-free update function does
void update(const unsigned long& value) noexcept {
    unsigned long tmp = S;
    while(HI32(S) < HI32(value) && 
            S.compare_exchange_weak(tmp, value)){}
// I thinks this is wrong
//    if (HI32(S) < HI32(value))
//        S = value; // this could be dangerous if we use threads! Why is that so? See 1 (race cond. between compare and write)

}

// initializes Knapsack problem
void init_problem() {

    // C++11 random number generator
    std::mt19937 engine(0);
    std::uniform_int_distribution<int> density(80, 100);
 
    // vector of ratios (needed for keysort)
    std::vector<Ratio> R;
   
    // generate a problem
    for (unsigned int i = 0; i < N; i++) {
        
        const unsigned int w = density(engine),
                           v = density(engine);

        W.push_back(w);
        V.push_back(v);
        R.push_back(Ratio(i, static_cast<float>(v)/static_cast<float>(w)));
    }
    
    // keysort R by ratio for upper bound estimate (we really need zip in C++)
    std::sort(R.begin(), R.end(), [] (const Ratio& a, const Ratio& b)
                                     { return a.second> b.second; });

    // rearrange V und W according to the indices of sorted R
    std::vector<unsigned int> newV(N);
    std::vector<unsigned int> newW(N);
    std::transform(R.begin(), R.end(), newV.begin(),
                   [] (const Ratio& x) { return V[x.first]; });
    std::transform(R.begin(), R.end(), newW.begin(),
                   [] (const Ratio& x) { return W[x.first]; });
    V = newV;
    W = newW;
}

unsigned int upper_bound(unsigned int height,   // height of the binary tree
                         unsigned int value,    // accumulated value until h
                         unsigned int weight) { // accumulated weight until h

    for (int index = height+1; index < N && weight <= C; index++) {
        value  += V[index];
        weight += W[index];
    }

    return value;
}

void printMyValues(const unsigned int height,        // height of the binary tree
                   const unsigned int old_value,     // accumulated value until h-1
                   const unsigned int old_weight, 
                   const unsigned int value,
                   const unsigned int weight,
                   int threadID,
                   const unsigned int valueReal) {

    std::cout << "\nThreadID, height, HI32(S), old_value, new_value, old_weight, new_weight, etc: " << threadID << ", " << height;
    std::cout << ", " << HI32(S) << ", " << old_value << ", " << value << ", " << old_weight;
    std::cout << ", " << weight << ", " << valueReal << std::endl;

}

void traverse(const unsigned int height,        // height of the binary tree
              const unsigned int old_value,     // accumulated value until h-1
              const unsigned int old_weight,    // accumulated weight until h-1
              const unsigned int state,
              int threadID) {       // encodes local state
    
   // if(height < N) {     
                /* 
    unsigned int status = state;
    unsigned int value = 0;
    unsigned int weight = 0;
    
    
    for (int i = 0; i <= height; i++) {
        bool bit = (status & 1);
        weight += bit*W[i];
        value  += bit*V[i];
        //std::cout << bit << "\t";
        status >>= 1;
    }
   */
    unsigned int status = state;
    unsigned int value3 = 0;
    unsigned int weight3 = 0;
    bool bit;
    if(height < 2 ) {
        for (int i = 0; i <= height; i++) {
            bool bit = (status & 1);
            weight3 += bit*W[i];
            value3  += bit*V[i];
            status >>= 1;
        }
        
    } else {
        value3 = old_value;
        weight3 = old_weight;
    
        bit = ((state>>height)&1);
        value3 += bit*V[height];
        weight3 += bit*W[height];
    }
    /*
    if((state>>height)&1) {
        value3 += V[height]; //*((state>>height)&1);
        weight3 += W[height]; //*((state>>height)&1);
    }
    */
    /*
    if(value3 != value && HI32(S) < 1500 ) {
        std::cout << "Value is wrong! ValueReal, ValueFalse " << value << ", " << value3 
        << " Thread " << threadID << ", height " << height << std::endl;
      //  std::cout << "Value is wrong!" << std::endl;
      //  printMyValues(height, old_value, old_weight, value, weight, threadID, value3);
    }
    if(weight3 != weight3 && HI32(S) < 1500 ) {
       std::cout << "Value is wrong! ValueReal, ValueFalse " << value << ", " << value3 
        << " Thread " << threadID << ", height " << height << std::endl;
    }
    */
    unsigned int value2 = value3;
    unsigned int weight2 = weight3;
    
  //  if(threadID != 5 && height == 1)
   //     printMyValues(height, old_value, old_weight, value, weight, threadID);
    //exceed capacity == terminate branch
    if(weight3 > C) return;
    //update global_upper_bound if necessary
    if(HI32(S) < value3){
       // if(threadID == 2) {
           // std::cout << "state >> height: " << (state>>height) << std::endl;
       // if(threadID == 4  && value <= 1500)
       //     printMyValues(height, old_value, old_weight, value, weight, threadID); 
        //if(threadID == 1 && value < 1000)
         //   printMyValues(height, old_value, old_weight, value, weight, threadID);  
        //    std::cout << "New: value, weight: " << value << ", " << weight << std::endl;
      //  }
         //if(value3 != value ) {
       //     std::cout << "Value is wrong! ValueReal, ValueFalse " << value << ", " << value3 << " Thread " << threadID << std::endl;
            
       // }
        unsigned long local_lower_bound = value3;
        local_lower_bound <<= 32;
        local_lower_bound += state;
        update(local_lower_bound);
     //   if(threadID == 2 )
       //     std::cout << "height, HI32(S), " << height << ", " << HI32(S) << std::endl;
        
    }
    //check upper bound greedily
    if(upper_bound(height,value3,weight3) <= HI32(S)) return;
    //recursive call
    traverse(height+1, value3, weight3, state, threadID);
    
    unsigned int status2 = state | (1 << (height+1));
    traverse(height+1, value2, weight2, status2, threadID);
    // I guess it's somehow recursive
    
}

void decompose_state(const unsigned long& state) {

    unsigned int value = 0;
    unsigned int weight = 0;
    unsigned int status = LO32(state);

    std::cout << std::endl << "best packing: " << std::endl << "S = ";
    for (int i = 0; i < N; i++) {
        bool bit = (status & 1);
        weight += bit*W[i];
        value  += bit*V[i];
        std::cout << bit << "\t";
        status >>= 1;
    }
    std::cout << std::endl;
    std::cout << "V = " << HI32(state) << " (Test: " << value
              << ") , W = " << weight << std::endl;
}

void sequential_solve() {

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    
    S = 0;

    traverse(0, 0, 0, 0, 0);
    traverse(0, 0, 0, 1, 0);
    
    end = std::chrono::system_clock::now();

    decompose_state(S);
    
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
               << elapsed_seconds.count()  << "s" << std::endl;
    
    
}

void parallel_solve() {
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    S = 0;

    std::thread t1 = std::thread(traverse, 1, V[0], W[0], 1, 1);
    std::thread t2 = std::thread(traverse, 1, 0, 0, 0, 2);

    //Traverse (height, oldvalue, oldweight, state)
    
    unsigned int oldValue = 0;
    unsigned int oldWeight = 0;
    
    oldValue += V[1];
    oldWeight += W[1];
    std::thread t3 = std::thread(traverse, 1, oldValue, oldWeight, 2, 3);
    oldValue += V[0];
    oldWeight += W[0];    
    std::thread t4 = std::thread(traverse, 1, oldValue, oldWeight, 3, 4); 
    
    /*
    oldValue = V[2];
    oldWeight = W[2];
    std::thread t5 = std::thread(traverse, 2, oldValue, oldWeight, 4); 
    
    
    oldValue += V[0];
    oldWeight += W[0];
    std::thread t6 = std::thread(traverse, 2, oldValue, oldWeight, 5); 
    
    oldValue += V[1];
    oldWeight += W[1];
    std::thread t7 = std::thread(traverse, 2, oldValue, oldWeight, 7); 
    
    oldValue = V[2];
    oldWeight = W[2];
    oldValue += V[1];
    oldWeight += W[1];
    std::thread t8 = std::thread(traverse, 2, oldValue, oldWeight, 6);    
    */

    
    t1.join();
    t2.join();
    
    t3.join();
    t4.join();
    /*
    t5.join();
    t6.join();
    t7.join();
    t8.join();
    */
    
    end = std::chrono::system_clock::now();

    decompose_state(S);

    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel h=1): "
               << elapsed_seconds.count()  << "s" << std::endl;
}


void parallel_solve_h(unsigned int h) {
    
    assert(0 < h && h < 32); 
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    S = 0;
    
    std::vector<unsigned int> weights (1 << h, 0);
    std::vector<unsigned int> values  (1 << h, 0);
    for (unsigned int i = 0; i < (1 << h); i++) {
        // mystic commands
        for(unsigned int j = 0; j < h; j++){
            //Ist hier eine if abfrage performanter? Bzw. wie teuer ist eine Multiplikation mit 1 bzw. 0?
            if((i>>j)&1){
                values[i] += V[j];//*((i>>j)&1);
                weights[i] += W[j];//*((i>>j)&1);
            }
        }
    }
    
    std::vector<std::thread> threads;
    for (unsigned int i = 0; i < (1 << h); i++)
        threads.push_back(std::thread(traverse, h-1, values[i], weights[i], i, 0)); 

    for (auto& thread : threads)
        thread.join();
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

    decompose_state(S);
    
    std::cout << "# elapsed time (parallel h=" <<  h << "): "
               << elapsed_seconds.count()  << "s" << std::endl;
}
////////////////////////////////////////////////////////////////////////////////

int main() {

    // constrains for the integer packing
    assert(sizeof(long) == 8);
    assert(N <= 32);

    init_problem();   // initialize V, W, 

    std::cout << "V = ";
    for (auto& v: V)
        std::cout << v << "\t";
    std::cout << std::endl;

    std::cout << "W = ";
    for (auto& w: W)
        std::cout << w << "\t";
    std::cout << std::endl;

    //sequential_solve();
    parallel_solve();
    /*
    for (int h = 2; h < 15; h++)
        parallel_solve_h(h);
    */
}