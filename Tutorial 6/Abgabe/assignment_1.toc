\contentsline {section}{1 Compare-and-Swap Loops (6P)}{3}
\contentsline {subsection}{(i) Explain why and where the error during the update of \texttt {global\_max} occurs.}{3}
\contentsline {subsection}{(ii) How would you resolve that problem using a mutex?}{3}
\contentsline {subsection}{(iii) Find an appropiate source explaining CAS-loops and the command \texttt {compare\_exchange\_weak}. Afterwards, implement a lock-free version of the stated code that produces the correct result.}{4}
\contentsline {section}{2 Solving the Knapsack Problem using a Static Schedule (14P)}{5}
\contentsline {subsection}{(i) Write a lock-free variant of the \texttt {update} method using a CAS-loop. If you cannot achieve this, keep the provided code. It will almost always produce the correct result.}{5}
\contentsline {subsection}{(ii) Implement the recursive \texttt {traverse} method. Prune unpromising candidates with the provided \texttt {upper\_bound} function. The result must be the same wether you use the upper bound or not.}{5}
\contentsline {subsection}{(iii) Benchmark the \texttt {sequential\_solve} (1 thread) and \texttt {parallel\_solve} (2 threads) methods. Using local upper bounds one obtains super-linear speedup. How can that be explained?}{6}
\contentsline {subsection}{(iv) Implement the method \texttt {parallel\_solve\_h} whch utilizes $2^h$ threads. To achieve that, one has to find an appropriate entry level in the tree. Moreover, one has to precompute the corresponding arguments of \texttt {traverse} to ensure correct results. Hint: binary decomposition.}{8}
\contentsline {subsection}{(v) Although the provided machine has only six physical cores (here we use 4 since Sauce gives Time-outs), the optimal speedup can be reached for the choice $h \approx 8 i.e. 2^8 = 256 $threads. Explain this counter-intuitive observation. How can this be possible although the majority of threads is not being executed simultaneously.}{9}
