void traverse(const unsigned int height,        // height of the binary tree
              const unsigned int old_value,     // accumulated value until h-1
              const unsigned int old_weight,    // accumulated weight until h-1
              const unsigned int state) {       // encodes local state
    
    unsigned int status = state;
    unsigned int value = 0;
    unsigned int weight = 0;
    bool bit;
   
    value = old_value;
    weight = old_weight;
    bit = ((state>>height)&1);
    value += bit*V[height];
    weight += bit*W[height];
    
    //exceed capacity == terminate branch
    if(weight > C) return;
    //update global_upper_bound if necessary
    if(HI32(S) < value){

        unsigned long local_lower_bound = value;
        local_lower_bound <<= 32;
        local_lower_bound += state;
        update(local_lower_bound);    
    }
    //check upper bound greedily
    if(upper_bound(height,value,weight) <= HI32(S)) return;
    //recursive call
    traverse(height+1, value, weight, state);
    traverse(height+1, value, weight, state | (1 << (height+1)));
    // I guess it's somehow recursive
}