void parallel_solve_h(unsigned int h) {
    
    assert(0 < h && h < 32); 
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    S = 0;
    
    std::vector<unsigned int> weights (1 << h, 0);
    std::vector<unsigned int> values  (1 << h, 0);
    for (unsigned int i = 0; i < (1 << h); i++) {
        // mystic commands
        unsigned int status = i;
        for(unsigned int j = 0; j < h-1; j++){

            //Ist hier eine if abfrage performanter? Bzw. wie teuer ist eine Multiplikation mit 1 bzw. 0?
            if((i>>j)&1){
                values[i] += V[j];//*((i>>j)&1);
                weights[i] += W[j];//*((i>>j)&1);
            }
            
        }
    }

    std::vector<std::thread> threads;
    for (unsigned int i = 0; i < (1 << h); i++)
        threads.push_back(std::thread(traverse, h-1, values[i], weights[i], i)); 

    for (auto& thread : threads)
        thread.join();
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

    decompose_state(S);
    
    std::cout << "# elapsed time (parallel h=" <<  h << "): "
               << elapsed_seconds.count()  << "s" << std::endl;
}