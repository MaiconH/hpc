void update(const unsigned long& value) noexcept {
    unsigned long tmp = S;
    while(HI32(S) < HI32(value) && 
            S.compare_exchange_weak(tmp, value)){}
}