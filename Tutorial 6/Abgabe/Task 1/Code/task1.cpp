std::atomic<unsigned int> global_max(0); // to be maximized concurrently

void update(const unsigned int p) 
{
    for(size_t i = 0; i < 100000; i++)
    {
        unsigned int value = (i&1)*i + p;
        
        if (value > global_max)
            global_max = value;
    }
}