void update(const unsigned int p) 
{
    for(size_t i = 0; i < 100000; i++)
    {
        unsigned int value = (i&1)*i + p;
        unsigned int tmp = global_max;
        // If global_max == tmp, then global_max has not been modified and
        // we can set it to value. Otherwise we safe the new global_max
        // in tmp and try again whether our value is still bigger or not.
        while(global_max < value && 
            !global_max.compare_exchange_weak(tmp, value)){}
    }
}