#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <omp.h>

#define N (1000)

double Riemann_Zeta(double s, int degree) {
    
    double result = 0.0;
    
    for (int i = 1; i < degree; i++)
        for (int j = 1; j < degree; j++)
            result += (2*(i&1)-1)/pow(i+j, s);

    return result*pow(2, s);
}
////////////////////////////////////////////////////////////////////////////////
// Begin task 2
int main () {
    
  
    std::vector<double> X(N);
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    
    for (unsigned int k = 0; k < N; k++)
        X[k] = Riemann_Zeta(2, k);
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
              << elapsed_seconds.count()  << "s" << std::endl;
    
    // hit me with your amazing benchmarks and schedules!

    std::cout << "I promise I will generate benchmarks in a scientific manner" << std::endl;

}
// End task 2
////////////////////////////////////////////////////////////////////////////////
