#include <algorithm>
#include <iostream>
#include <random>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <fstream>

#define ROUNDS (1000)

void init(std::vector<unsigned int>& X) {

    std::mt19937 engine (42);
    std::uniform_int_distribution<> distribution(0, X.size());

    for (auto& x : X)
        x = distribution(engine);

}

void sequential_sort(std::vector<unsigned int>& X) {
    
    unsigned int i, j, count, N = X.size();
    std::vector<unsigned int > tmp(N);

    for (i = 0; i < N; i++) {
        count = 0;
        for (j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i)
                count++;
            tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}
////////////////////////////////////////////////////////////////////////////////
// Begin task 1
void parallel_sort(std::vector<unsigned int>& X) {
     
    unsigned int N = X.size();
    std::vector<unsigned int > tmp(N);
    
    #pragma omp parallel for 
    for (unsigned int i = 0; i < N; i++) {
        unsigned int count = 0;
        #pragma omp parallel for reduction(+:count)
        for (unsigned int j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i) 
                count++;
        tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}


void parallel_sort_one(std::vector<unsigned int>& X) {
     
    unsigned int N = X.size();
    std::vector<unsigned int > tmp(N);
    
    #pragma omp parallel for 
    for (unsigned int i = 0; i < N; i++) {
        unsigned int count = 0;
        
        for (unsigned int j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i) 
                count++;
        tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}

void parallel_sort_second(std::vector<unsigned int>& X) {
     
    unsigned int N = X.size();
    std::vector<unsigned int > tmp(N);
    
    for (unsigned int i = 0; i < N; i++) {
        unsigned int count = 0;
        #pragma omp parallel for reduction(+:count)
        for (unsigned int j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i) 
                count++;
        tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}
// End task 1
////////////////////////////////////////////////////////////////////////////////
bool check (const std::vector<unsigned int>& X) {
    return std::is_sorted(X.begin(), X.end());
}

int main () {
    
    std::ofstream task1Bench;
    task1Bench.open ("task1Bench", std::ios_base::app);
    task1Bench << "Sequential;Parallel2;Parallel1;ParallelSecond\n";
    task1Bench.close();
    
    for (unsigned int i = 0; i < ROUNDS; i++) {
        
        std::vector<unsigned int> X(50000);

        init(X);
    
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();

        sequential_sort(X);
    
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        
        task1Bench.open ("task1Bench", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";";
        task1Bench.close();

        init(X);
    
        start = std::chrono::system_clock::now();
    
        parallel_sort(X);
       
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        
        task1Bench.open ("task1Bench", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";";
        task1Bench.close();
        
        init(X);
    
        start = std::chrono::system_clock::now();
    
        parallel_sort_one(X);
       
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        
        task1Bench.open ("task1Bench", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";";
        task1Bench.close();
        
        init(X);
    
        start = std::chrono::system_clock::now();
    
        parallel_sort_second(X);
       
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        
        task1Bench.open ("task1Bench", std::ios_base::app);
        task1Bench << elapsed_seconds.count() << ";\n";
        task1Bench.close();
    }  
    std::cout << "Programme terminated." << std::endl;
}
 
