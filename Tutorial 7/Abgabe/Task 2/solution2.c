#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <omp.h>
#include <iostream>
#include <fstream>

#define N (1000)
#define THREAD_COUNT (4)
#define ROUNDS (1000)

double Riemann_Zeta(double s, int degree) {
    
    double result = 0.0;
    
    for (int i = 1; i < degree; i++)
        for (int j = 1; j < degree; j++)
            result += (2*(i&1)-1)/pow(i+j, s);

    return result*pow(2, s);
}
////////////////////////////////////////////////////////////////////////////////
// Begin task 2
int main () {
    std::ofstream task2Bench;
    task2Bench.open ("task2Bench", std::ios_base::app);
    task2Bench << "Sequential;Static with chunk size; Static interleaved;Dynamic;Guided;Auto;\n";
    task2Bench.close();
    
    
    for (unsigned int i = 0; i < ROUNDS; i++) {
        
        std::vector<double> X(N);  
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        
        for (unsigned int k = 0; k < N; k++)
            X[k] = Riemann_Zeta(2, k);
 
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        task2Bench.open ("task2Bench", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
   
              
        // Static parallelization
        std::vector<double> Y(N); 
        start = std::chrono::system_clock::now();
        
        #pragma omp parallel for schedule(static) num_threads(THREAD_COUNT) 
        for (unsigned int k = 0; k < N; k++)
            Y[k] = Riemann_Zeta(2, k);
    
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2Bench", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
        
        
        // Static interleaved parallelization
        std::vector<double> YA(N); 
        start = std::chrono::system_clock::now();
        
        #pragma omp parallel for schedule(static, 1) num_threads(THREAD_COUNT) 
        for (unsigned int k = 0; k < N; k++)
            YA[k] = Riemann_Zeta(2, k);
    
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2Bench", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
    
              
        // Dynamic approach
        std::vector<double> Z(N);     
        start = std::chrono::system_clock::now();
    
        #pragma omp parallel for schedule(dynamic) num_threads(THREAD_COUNT)
        for (unsigned int k = 0; k < N; k++)
            Z[k] = Riemann_Zeta(2, k);
    
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2Bench", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();
                  
     
        // Guided approach
        std::vector<double> ZA(N);
        start = std::chrono::system_clock::now();
    
        #pragma omp parallel for schedule(guided) num_threads(THREAD_COUNT)
        for (unsigned int k = 0; k < N; k++)
            ZA[k] = Riemann_Zeta(2, k);
    
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2Bench", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";";
        task2Bench.close();

              
        // Auto approach
        std::vector<double> ZB(N); 
        start = std::chrono::system_clock::now();
    
        #pragma omp parallel for schedule(guided) num_threads(THREAD_COUNT)
        for (unsigned int k = 0; k < N; k++)
            ZB[k] = Riemann_Zeta(2, k);
    
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        task2Bench.open ("task2Bench", std::ios_base::app);
        task2Bench << elapsed_seconds.count() << ";\n";
        task2Bench.close();
    }
    std::cout << "programme terminated" << std::endl;
}
// End task 2
////////////////////////////////////////////////////////////////////////////////
