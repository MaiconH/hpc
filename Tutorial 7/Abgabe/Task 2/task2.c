double Riemann_Zeta(double s, int k) {
    double result = 0.0;
    
    for (int i = 1; i < k; i++)
        for (int j = 1; j < k; j++)
            result += (2*(i&1)-1)/pow(i+j, s);
        
    return result*pow(2, s);
}

// Some other function:
    for (unsigned int k = 0; k < N; k++)
        X[k] = Riemann_Zeta(2, k); // = pi^2/6