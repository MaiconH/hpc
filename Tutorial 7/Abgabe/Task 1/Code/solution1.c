#include <algorithm>
#include <iostream>
#include <random>
#include <chrono>
#include <omp.h>

void init(std::vector<unsigned int>& X) {

    std::mt19937 engine (42);
    std::uniform_int_distribution<> distribution(0, X.size());

    for (auto& x : X)
        x = distribution(engine);

}

void sequential_sort(std::vector<unsigned int>& X) {
    
    unsigned int i, j, count, N = X.size();
    std::vector<unsigned int > tmp(N);

    for (i = 0; i < N; i++) {
        count = 0;
        for (j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i)
                count++;
            tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}
////////////////////////////////////////////////////////////////////////////////
// Begin task 1
void parallel_sort(std::vector<unsigned int>& X) {
     
    unsigned int N = X.size();
    std::vector<unsigned int > tmp(N);
    
    // There are two more possibilities: Only the outer loop parallelized or
    // only the inner loop parallelized. See benchmarks for comparison.
    #pragma omp parallel for 
    for (unsigned int i = 0; i < N; i++) {
        unsigned int count = 0;
        #pragma omp parallel for reduction(+:count)
        for (unsigned int j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i) 
                count++;
        tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}
// End task 1
////////////////////////////////////////////////////////////////////////////////
bool check (const std::vector<unsigned int>& X) {
    return std::is_sorted(X.begin(), X.end());
}

int main () {
    
    std::vector<unsigned int> X(50000);

    init(X);
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    sequential_sort(X);
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
              << elapsed_seconds.count()  << "s" << std::endl;
    
    std::cout << (check(X) ? "passed" : "failed") << std::endl;

    init(X);
    
    start = std::chrono::system_clock::now();
    
    parallel_sort(X);
    
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel): "
              << elapsed_seconds.count()  << "s" << std::endl;
    
    std::cout << (check(X) ? "passed" : "failed") << std::endl;

}
