\contentsline {section}{1 Parallelize Sorting with OpenMP (10P)}{3}
\contentsline {subsection}{(i) Explain how this algorithm works.}{3}
\contentsline {subsection}{(ii) Analyse the data dependencies of each loop. Which loop is ideally suited for a parallelization with OpenMP pragmas? Consider the problem of shared variables.}{3}
\contentsline {subsection}{(iii) Implement a parallel version of \texttt {sequential\_sort} according to your former considerations. Discuss speedup and efficiency.}{4}
\contentsline {section}{2 Scheduling Loops with OpenMP (10P)}{9}
\contentsline {subsection}{(i) Parallelize this loop using OpenMP. Are there any data dependencies or shared variables?}{9}
\contentsline {subsection}{(ii) Talk about the load balancing of the individual threads. You can assume \texttt {NUM\_THREADS = 6}.}{11}
\contentsline {subsection}{(iii) Discuss different scheduling modes and the corresponding runtimes.}{12}
