 
#include <algorithm>
#include <iostream>
#include <random>
#include <chrono>
#include <omp.h>

void init(std::vector<unsigned int>& X) {

    std::mt19937 engine (42);
    std::uniform_int_distribution<> distribution(0, X.size());

    for (auto& x : X)
        x = distribution(engine);

}

void sequential_sort(std::vector<unsigned int>& X) {
    
    unsigned int i, j, count, N = X.size();
    std::vector<unsigned int > tmp(N);

    for (i = 0; i < N; i++) {
        count = 0;
        for (j = 0; j < N; j++)
            if (X[j] < X[i] || X[j] == X[i] && j < i)
                count++;
            tmp[count] = X[i];
    }
    
    std::copy(tmp.begin(), tmp.end(), X.begin());
}
////////////////////////////////////////////////////////////////////////////////
void parallel_sort(std::vector<unsigned int>& X) {
    // your magic here
}

////////////////////////////////////////////////////////////////////////////////
bool check (const std::vector<unsigned int>& X) {
    return std::is_sorted(X.begin(), X.end());
}

int main () {
    
    std::vector<unsigned int> X(50000);

    init(X);
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    sequential_sort(X);
    
    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "# elapsed time (sequential): "
              << elapsed_seconds.count()  << "s" << std::endl;
    
    std::cout << (check(X) ? "passed" : "failed") << std::endl;

    init(X);
    
    start = std::chrono::system_clock::now();
    
    parallel_sort(X);
    
    end = std::chrono::system_clock::now();
    elapsed_seconds = end-start;
    std::cout << "# elapsed time (parallel): "
              << elapsed_seconds.count()  << "s" << std::endl;
    
    std::cout << (check(X) ? "passed" : "failed") << std::endl;

}
