\contentsline {section}{1 Puzzling Primes (6P)}{3}
\contentsline {subsection}{(i) Prove that the operation $x \odot y := x+y+x \cdot y$ is commutative and associative.}{3}
\contentsline {subsection}{(ii) Using the result of (i), how can we efficiently parallelize this algorithm?}{3}
\contentsline {subsection}{(iii) Investigate the runtime of the algorithm on p processor. Discuss the result.}{3}
\contentsline {section}{2 Poisson Equation Solver using 2D Jacobi Iteration (14P)}{4}
\contentsline {subsection}{(i) Explaining the indexing scheme.}{4}
\contentsline {subsection}{(ii) Why do we need the customized column data type COL? Provide an alternative naive solution.}{4}
\contentsline {subsection}{(iii) Implement the communication between square tiles.}{5}
\contentsline {subsection}{(iv) Why is it mandatory to call MPI\_Allreduce(...) after each iteration? Please note, the answer is more complicated than to ensure a correct value for ).}{9}
\contentsline {subsection}{(v) The proposed scheme will always converge no matter how good or erroneous your communication and update calls are. As a result, you should really inspect the provided images and check for consistency. The image should be smooth, tile borders should not be visible $\epsilon $.}{9}
