void parallel_jacobi(int N, int size, int rank, 
                     std::string filename, float thr=1E-100) {

    // check for divisability
    if (size != 4 && size != 9 && size != 16 && size != 25) {
            // exit computation with error message
            if (rank == 0)
                std::cout << "-np must be in {4, 9, 16, 25}" << std::endl;
            return;
    }
    
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    
    ///////////////////////////////////////////////////////////////////////////
    // initialize all the things!
    ///////////////////////////////////////////////////////////////////////////
    
    // global copies for rho phi and tmp
    float * rho = new float[N*N]; // charge density
    float * phi = new float[N*N]; // electric potential
    float * tmp = new float[N*N]; // temporary storage during iteration

    // forget about communication of the global arrays, we don't broadcast zeros
    init_rho(rho, N);
    init_phi(phi, N);
    init_phi(tmp, N);
    
    // for convenient indexing of tiles
    int L = sqrt(size); // number of tiles in one dimension
    int M = (N-2) / L;  // length of a tile
    int s = rank / L;   // row index of tile
    int t = rank % L;   // col index of tile
    
    // data type for sending columns with global MPI collective calls
    MPI_Datatype TMP, COL;
    MPI_Type_vector(M, 1, N, MPI_FLOAT, &TMP);
    MPI_Type_create_resized(TMP , 0, sizeof(float), &COL);
    MPI_Type_commit(&COL);

    // one status and request handler for each communication
    MPI_Status * status = new MPI_Status[8*size];
    MPI_Request * request = new MPI_Request[8*size];

    ///////////////////////////////////////////////////////////////////////////
    // the actual computation 
    ///////////////////////////////////////////////////////////////////////////
    
    // initial threshold
    float eps = INFINITY;
    
    // determine offset for border control
    int loi = (s != 0);
    int upi = (s != L-1);
    int loj = (t != 0);
    int upj = (t != L-1);

    // explain the indices
    int lower = 1+s*M;
    int upper = 1+(s+1)*M;
    int left  = 1+t*M;
    int right = 1+(t+1)*M;
    while (eps > thr) {
        //////////////////////////////////////////////////////////////////////
        // state sends and receives
        //////////////////////////////////////////////////////////////////////
        // Boundaries are 0
        if (upi) {
            float * ptr_lst_row = tmp+(upper-1)*N+left;
            float * ptr_nxt_row = tmp+(upper+0)*N+left;
            int node_below = rank + L;
           // std::cout << "ptr_lst_row " << (upper-1)*N+left << " ptr_nxt_row " 
            //    << (upper+0)*N+left << " rank " << rank << std::endl;
            MPI_Isend(ptr_lst_row, M, MPI_FLOAT, node_below, 0, MPI_COMM_WORLD, request+rank);
            MPI_Irecv(ptr_nxt_row, M, MPI_FLOAT, node_below, 0, MPI_COMM_WORLD, request+rank+size);   
        }
        
        if (loi) {
            float * ptr_prv_row = tmp+(lower-1)*N+left;
            float * ptr_fst_row = tmp+(lower-0)*N+left;
            int node_above = rank - L;
           // std::cout << "ptr_prv_row " << (lower-1)*N+left << " ptr_fst_row " 
            //    << (lower+0)*N+left << " rank " << rank << std::endl;
            MPI_Isend(ptr_fst_row, M, MPI_FLOAT, node_above, 0, MPI_COMM_WORLD, request+rank+size*2);
            MPI_Irecv(ptr_prv_row, M, MPI_FLOAT, node_above, 0, MPI_COMM_WORLD, request+rank+size*3);    
        }

        if (upj) {
            float * ptr_lst_col = tmp+lower*N+right-1;
            float * ptr_nxt_col = ptr_lst_col+1;
            int node_right = rank + 1;
            //std::cout << "ptr_lst_col " << lower*N+right-1 << " ptr_nxt_col " 
           //     << lower*N+right << " rank " << rank << std::endl;
            MPI_Isend(ptr_lst_col, M, MPI_FLOAT, node_right, 0, MPI_COMM_WORLD, request+rank+size*4);
            MPI_Irecv(ptr_nxt_col, M, MPI_FLOAT, node_right, 0, MPI_COMM_WORLD, request+rank+size*5);
        }

        if (loj) {
            float * ptr_prv_col = tmp+lower*N+left-1;
            float * ptr_fst_col = ptr_prv_col+1;
            int node_left = rank - 1;
           // std::cout << "ptr_prv_col " << lower*N+left-1 << " ptr_fst_col " 
           //     << lower*N+left << " rank " << rank << std::endl;
            MPI_Isend(ptr_fst_col, M, MPI_FLOAT, node_left, 0, MPI_COMM_WORLD, request+rank+size*6);
            MPI_Irecv(ptr_prv_col, M, MPI_FLOAT, node_left, 0, MPI_COMM_WORLD, request+rank+size*7);   
        }

        //////////////////////////////////////////////////////////////////////
        // update the interiour
        //////////////////////////////////////////////////////////////////////
        for (int i = lower + (!loi); i < upper - (!upi); i++)
            for (int j = left + (!loj); j < right - (!upj); j++) 
                update(rho, phi, tmp, i, j, N);
        
        //////////////////////////////////////////////////////////////////////
        // wait until communication done (MPI_Wait)
        //////////////////////////////////////////////////////////////////////
        if (loi) {
            MPI_Wait(&request[rank+size*3], &status[rank+size*3]);
        }

        if (upi) {
            MPI_Wait(&request[rank+size], &status[rank+size]);
        }

        if (loj) {
            MPI_Wait(&request[rank+size*7], &status[rank+size*7]);
        }

        if (upj) {
            MPI_Wait(&request[rank+size*5], &status[rank+size*5]);
        }

            
        //////////////////////////////////////////////////////////////////////
        // update halo pixels
        //////////////////////////////////////////////////////////////////////
        
        if (loi) {
            // update lanes
            //std::cout << "# loi upper " << upper << " lower " 
            //    << lower << " rank " << rank << std::endl;
            for (int j = left + (!loj); j < right - (!upj); j++) 
                update(rho, phi, tmp, lower, j, N);
        }
        
        if (upi) {
            // update lanes
           // std::cout << "# upi upper " << upper-1 << " lower " 
            //    << lower << " rank " << rank << std::endl;
            for (int j = left + (!loj); j < right - (!upj); j++) 
                update(rho, phi, tmp, upper-1, j, N);
        }
        
        if (loj) {
            // update lanes
           // std::cout << "# loj right " << right << " left " 
           //     << left << " rank " << rank << std::endl;
            for (int i = lower + (!loi); i < upper - (!upi); i++)
                update(rho, phi, tmp, i, left, N);
        }
        
        if (upj) {
            // update lanes
           // std::cout << "# upj right " << right-1 << " left " 
            //    << left << " rank " << rank << std::endl;
            for (int i = lower + (!loi); i < upper - (!upi); i++) 
                update(rho, phi, tmp, i, right-1, N);
        }
       
        //////////////////////////////////////////////////////////////////////
        // now update error of approximation
        //////////////////////////////////////////////////////////////////////
        
        eps = error_copy(phi, tmp, N,
                         lower-loi, upper+upi, left-loj, right+upj);
        
        // make sure all threads have the same epsilon (why is this important?)
        MPI_Allreduce(&eps, &eps, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
        
        // for the curious guys
        /*
        if (rank == 0)
            std::cout << "# error = " << eps << std::endl;
        */
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // now, we are done with the calculation
    ///////////////////////////////////////////////////////////////////////////
   
    // submatrix data type for global MPI collective calls
    MPI_Datatype TMP2, BLOCK;
    MPI_Type_vector(M, M, N, MPI_FLOAT, &TMP2);
    MPI_Type_create_resized(TMP2 , 0, sizeof(float), &BLOCK);
    MPI_Type_commit(&BLOCK);

    // offset calculation (explain that!)
    int * dn = new int[size];
    int * dP = new int[size];
    
    for (int i = 0; i < size; i++) {
        dn[i] = 1;
        dP[i] = N+1 + (i/L)*M*N + (i%L)*M; // explain each summand
    }
    
   
    // I hope, you know whats happening here (explain that, too!)
    MPI_Gatherv(tmp+dP[rank], 1, BLOCK, phi, dn, dP, BLOCK, 0, MPI_COMM_WORLD);
    
    if (rank == 0) {
    
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::cout << "# elapsed time (parallel): "
                  << elapsed_seconds.count()  << "s" << std::endl;
    
        dump(phi, N, filename);
    }
    
    
    
    ///////////////////////////////////////////////////////////////////////////
    // clean up
    ///////////////////////////////////////////////////////////////////////////
    
    MPI_Type_free(&COL);
    MPI_Type_free(&BLOCK);
    MPI_Type_free(&TMP);
    MPI_Type_free(&TMP2);
    
    delete [] status;
    delete [] request;
    
    delete [] dn;
    delete [] dP;
    
    delete [] rho;
    delete [] phi;
    delete [] tmp;
}