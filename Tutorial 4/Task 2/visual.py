import numpy as np
import pylab as pl
import array as ar
import sys

# to make this work
# sudo apt-get install python-numpy python-scipy python-matplotlib mayavi2

filename = sys.argv[1]

with open(filename+"_seq.bin", "rb") as f:
    data_seq = np.array(ar.array("f", f.read()))
    N = int(len(data_seq)**0.5)
    data_seq = data_seq.reshape((N, N))

with open(filename+"_mpi.bin", "rb") as f:
    data_mpi = np.array(ar.array("f", f.read()))
    N = int(len(data_mpi)**0.5)
    data_mpi = data_mpi.reshape((N, N))


pl.subplot(131)
pl.title("Sequential (reference)")
pl.imshow(data_seq, interpolation="nearest", origin='lower')
pl.colorbar(fraction=0.046, pad=0.04)

pl.subplot(132)
pl.title("MPI (your code)")
pl.imshow(data_mpi, interpolation="nearest", origin='lower')
pl.colorbar(fraction=0.046, pad=0.04)

pl.subplot(133)
pl.title("squared residues")
pl.imshow((data_mpi-data_seq)**2, interpolation="nearest", origin='lower')
pl.colorbar(fraction=0.046, pad=0.04)

pl.tight_layout()
pl.savefig(filename)

